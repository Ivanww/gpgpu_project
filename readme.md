# Bayesian Optimization Framework with Gaussian Process Implemented By CUDA

*Course Project: General Purpose GPU*

### Project Overview

* This project aims to implementing Gaussian Process Regression (GPR) with Bayesian optimization with CUDA

* Gaussian Process is a stochastic model. It assumes any finite set values of target function following multivariate Gaussian distribution

* Bayesian optimization procedure takes it as a delegate then determine a new position with largest probability of being a maximal

* Iteratively, new sample is sent to GPR to improve model accuracy

* Hopefully proposed location will converge to a small neighbor area, then that’s what we are looking for

  ![](images/overview.png)

### Functions implemented with CUDA

* Square exponential kernel
  $$
  K(x, x')=\sigma^2\exp(-\frac{(x-x')^T(x-x')}{2l^2})
  $$

* Partial gradient of $\sigma$ with respect to $K$
  $$
  \frac{\partial K}{\partial \sigma }=2\sigma\exp(-\frac{(x-x')^T(x-x')}{2l^2})
  $$

* Partial gradient of $l$ with respect to $K$
  $$
  \frac{\partial K}{\partial l}=\sigma^2\exp(-\frac{(x-x')^T(x-x')}{2l^2})\frac{(x-x')^T(x-x')}{l^3}
  $$

* Log likelihood of Gaussian
  $$
  \ln P(y|x, \theta)=-\frac{1}{2}\ln |K|-\frac{1}{2}y^TK^{-1}y-\frac{N}{2}\ln2\pi
  $$

* Gaussian Process predictive mean
  $$
  \mu=K(X,x')K(X,X)y
  $$

* Gaussian Process predictive standard variance 
  $$
  \sigma^2=K(X, x)^TK(X, X)^{-1}K(X,x)
  $$
  

### Project Roadmap

**Prototypes**

* Python prototype to demonstrate
* C++ prototype to verify nonlinear optimization

**CUDA Implementation**

* Move matrix operations to GPU
* cuBLAS/cuSover subroutines + customized kernels

### Implementation Details and Profiling

**Gaussian Process Kernel Computation**

For example, in (1), $x, x'$ can be vector or matrix. Use cuda stream to parallel matrix-vector computation; reuse one auxiliary space to buffer the results from different rows.

![](images/matrix-vector.png)

**Profiling**

Matrix-Vector Kernel Computation

![](images/prof-matrix-kernel.png)

Matrix-Matrix Kernel Computation

![](images/prof-matrix-matrix.png)

**Gaussian Process Kernel Inverse**

* Kernel inverse = Cholesky decomposition + solving linear matrix

* Overlap CUDA kernel computing with data transferring

**Profiling**

![](images/matrix-inverse.png)

### Evaluation

In this project, each function is carefully verified with reusable benchmarks

![](images/evaluation.png)

* First two rows show the result from python prototypes after each iteration.
  * The first row shows the object function (yellow), sampling point (black points), estimating function (blue). With more shaping points, the estimating function is approaching object function.
  * The middle row shows the acquisition function. The position of the peak of acquisition unction is proposed to be the next sampling function. With the iteration, the peak position is converging to the maximal value of the object function.
* Rightmost row shows the result from CUDA implementation. Our implementation also converges to the maximal position as prototype.

### Known Issues

* In this implementation, we used the the C++ optimization library for gradient-based optimization. It causes huge GPU overhead.
* Due to the overhead of “malloc” and “free” space, its speed performance cannot be properly evaluated for now.
* Optimization runs on CPU, data transferring is too heavy

Profiling of one optimization iteration

![](images/overhead.png)

### Conclusion

**Highlights**

* Implementing all components of a Gaussian Process Optimization procedure.
* All function are carefully verified. Reusable testing benchmark
* Involving some parallel computing with CUDA C++

**Lowlights**

* Data transferring is too heavy as nonlinear optimization still runs on CPU
* Spent too much time on numerical stability issues

### Implementation Dependency

**C++ Libraries**

* NLopt (https://nlopt.readthedocs.io/en/latest/): an open-source library for nonlinear optimization
* Armadillo (http://arma.sourceforge.net/): a C++ linear algebra library (for result verification)

**CUDA Libraries**

* cuBLAS: (https://docs.nvidia.com/cuda/cublas): a CUDA implementation of Basic Linear Algebra Subprograms
* cuSOLVER (https://docs.nvidia.com/cuda/cusolver/index.html): a high-level package providing common matrix factorization