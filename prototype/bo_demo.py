# this is a prototype demo of Bayesion Optimization in GP
## Follow the the reference: http://krasserm.github.io/2018/03/21/bayesian-optimization/
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.optimize import minimize

## some util funcitons

def plot_approximation(gpr, X, Y, X_sample, Y_sample, X_next=None, show_legend=False):
    mu, std = gpr.predict(X, return_std=True)
    print(mu.shape, std.shape)
    plt.fill_between(X.ravel(), 
                     mu.ravel() + 1.96 * std, 
                     mu.ravel() - 1.96 * std, 
                     alpha=0.1) 
    plt.plot(X, Y, 'y--', lw=1, label='Noise-free objective')
    plt.plot(X, mu, 'b-', lw=1, label='Surrogate function')
    plt.plot(X_sample, Y_sample, 'kx', mew=3, label='Noisy samples')
    if X_next:
        plt.axvline(x=X_next, ls='--', c='k', lw=1)
    if show_legend:
        plt.legend()

def plot_acquisition(X, Y, X_next, show_legend=False):
    plt.plot(X, Y, 'r-', lw=1, label='Acquisition function')
    plt.axvline(x=X_next, ls='--', c='k', lw=1, label='Next sampling location')
    if show_legend:
        plt.legend()    

def plot_convergence(X_sample, Y_sample, n_init=2):
    plt.figure(figsize=(12, 3))

    x = X_sample[n_init:].ravel()
    y = Y_sample[n_init:].ravel()
    r = range(1, len(x)+1)
    
    x_neighbor_dist = [np.abs(a-b) for a, b in zip(x, x[1:])]
    y_max_watermark = np.maximum.accumulate(y)
    
    plt.subplot(1, 2, 1)
    plt.plot(r[1:], x_neighbor_dist, 'bo-')
    plt.xlabel('Iteration')
    plt.ylabel('Distance')
    plt.title('Distance between consecutive x\'s')

    plt.subplot(1, 2, 2)
    plt.plot(r, y_max_watermark, 'ro-')
    plt.xlabel('Iteration')
    plt.ylabel('Best Y')
    plt.title('Value of best selected sample')

# A function
def func(X, noise=0.2):
    return -np.sin(3*X) - X**2 + 0.7*X + noise * np.random.randn(*X.shape)

def show_function():
    bounds = np.array([[-1., 2.]])
    X = np.arange(bounds[:, 0], bounds[:, 1], 0.01).reshape(-1, 1)

    Y = func(X, 0)

    plt.plot(X, Y, 'y--', lw=2, label='Noise-free objective')
    plt.plot(X, func(X), 'bx', lw=1, label='Noisy Samples')
    plt.plot(X, Y, 'y--', lw=2, label='Noise-free objective')

    plt.legend()
    plt.show()

# Expected improvement function
def EI(X, X_sample, Y_sample, gpr, xi=0.01):
    mu, sigma = gpr.predict(X, return_std=True)
    mu_sample = gpr.predict(X_sample)

    sigma = sigma.reshape(-1, X_sample.shape[1])

    mu_sample_opt = np.max(mu_sample)

    with np.errstate(divide='warn'):
        imp = mu - mu_sample_opt - xi
        Z = imp / sigma
        ei = imp * norm.cdf(Z) + sigma * norm.pdf(Z)
        ei[sigma == 0.0] = 0.0

    return ei

# Function to propose next location
def next_location(acquisition, X_sample, Y_sample, gpr, bounds, n_restarts=25):
    dim = X_sample.shape[1]
    min_val = 1
    min_x = None

    def min_obj(X):
        print(X)
        return -acquisition(X.reshape(-1, dim), X_sample, Y_sample, gpr)
    
    for x0 in np.random.uniform(bounds[:, 0], bounds[:, 1], size=(n_restarts, dim)):
        res = minimize(min_obj, x0=x0, bounds=bounds, method='L-BFGS-B')

        if res.fun < min_val:
            min_val = res.fun[0]
            min_x = res.x

    return min_x.reshape(-1, 1)

# Bayesian Optimization Demo on sklearn's GP model
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import ConstantKernel, Matern
def show_bo_demo():
    noise = 0.2
    bounds = np.array([[-1., 2.]])
    X = np.arange(bounds[:, 0], bounds[:, 1], 0.01).reshape(-1, 1)
    Y = func(X, 0)
    m52 = ConstantKernel(1.) * Matern(length_scale=1., nu=2.5)
    gpr = GaussianProcessRegressor(kernel=m52, alpha=noise**2)

    X_sample = np.array([[-0.9], [1.1]])
    Y_sample = func(X_sample)

    n_iter = 10

    plt.figure(figsize=(12, n_iter*3))
    plt.subplots_adjust(hspace=0.4)

    for i in range(n_iter):
        gpr.fit(X_sample, Y_sample)

        X_next = next_location(EI, X_sample, Y_sample, gpr, bounds)

        Y_next = func(X_next, noise=noise)

        plt.subplot(n_iter, 2, 2*i+1)
        plot_approximation(gpr, X, Y, X_sample, Y_sample, show_legend=(i==0))
        plt.title(f'Iteration {i+1}')

        plt.subplot(n_iter, 2, 2*i+2)
        plot_acquisition(X,EI(X, X_sample, Y_sample, gpr), X_next, show_legend=(i==0))

        X_sample = np.vstack((X_sample, X_next))
        Y_sample = np.vstack((Y_sample, Y_next))

    plt.show()

if __name__ == "__main__":
    show_bo_demo()