import numpy as np
from scipy.optimize import minimize
# Gaussian Process
from numpy.linalg import inv, cholesky, solve
# Bayesian optimization
from scipy.stats import norm

# some utilities function
import matplotlib.pyplot as plt

def plot_gp(mu, cov, X, X_train=None, Y_train=None, samples=[]):
    X = X.ravel()
    mu = mu.ravel()

    uncertainty = 1.96 * np.sqrt(np.diag(cov))

    plt.fill_between(X, mu+uncertainty, mu-uncertainty, alpha=0.1)
    plt.plot(X, mu, label='Mean')

    for i, sample in enumerate(samples):
        plt.plot(X, sample, lw=1, ls='--', label=f'Sample {i+1}')

    if X_train is not None:
        plt.plot(X_train, Y_train, 'rx ')

    plt.legend()

def plot_approx(gpr, X, Y, X_sample, Y_sample, X_next=None, show_legend=False):
    mu, std = gpr.predict(X, std=True)
    plt.fill_between(X.ravel(), mu.ravel() + 1.96 * std, mu.ravel() - 1.96 * std, alpha=0.1)
    plt.plot(X, Y, 'y--', lw=1, label='Noise-free Objective')
    plt.plot(X, mu, 'b-', lw=1, label='Surrogate Function')
    plt.plot(X_sample, Y_sample, 'kx', mew=3, label='Noisy Samples')

    if X_next:
        plt.axvline(x=X_next, ls='--', c='k', lw=1)
    
    if show_legend:
        plt.legend()

def plot_acquisition(X, Y, X_next, show_legend=False):
    plt.plot(X, Y, 'r-', lw=1, label='Acquisition Function')
    plt.axvline(x=X_next, ls='--', c='k', lw=1, label='Next Sampling Location')

    if show_legend:
        plt.legend()

# kernel function
class RBFKernel():
    def __init__(self, params, bounds=((1e-5, None), (1e-5, None))):
        self._params = params
        self._bounds = bounds
        self._l, self._sigma_f = params
    
    @property
    def parameters(self):
        return self._params
    
    @parameters.setter
    def parameters(self, params):
        self._params = params
        self._l, self._sigma_f = params
    
    @property
    def bounds(self):
        return self._bounds
    
    @bounds.setter
    def bounds(self, bounds):
        self._bounds = bounds

    def __call__(self, X1, X2, l=None, sigma_f=None):
        return self.call(X1, X2, l, sigma_f)

    def call(self, X1, X2, l=None, sigma_f=None):
        ''' To compute the kernel of X and Y 
        k(X1, X2) = \sigma_f^2\exp(-\frac{1}{2l^2}(X_1-X_2)^T(X_i-X_j))
        '''
        sqdist = np.sum(X1**2, 1).reshape((-1, 1)) + np.sum(X2**2, 1) - 2*np.dot(X1, X2.T)
        if l is None or sigma_f is None:
            return self._sigma_f**2 * np.exp(-0.5 / self._l**2 * sqdist)
        else:
            return sigma_f**2 * np.exp(-0.5 / l**2 * sqdist)

class GaussianProcess():
    def __init__(self, kernel, X=None, Y=None, noise=1e-8):
        self.kernel = kernel
        self.noise = noise 
        self.X = X
        self.Y = Y

        if self.X is not None and self.Y is not None:
            self._mu, self._sigma = self.predict(self.X, cov=True)
        else:
            self._mu, self._sigma = None, None
    
    @property
    def mu(self):
        return self._mu
    
    @mu.setter
    def mu(self, m):
        self._mu = m
    
    @property
    def sigma(self):
        return self._sigma
    
    @sigma.setter
    def sigma(self, s):
        self._sigma = s

    def fit(self, X=None, Y=None):
        ''' This method optimize parameters according to provided X and Y '''
        if X is not None and Y is not None:
            self.X = X
            self.Y = Y

        def nll(params):
            K = self.kernel(self.X, self.X, l=params[0], sigma_f=params[1]) + \
                self.noise**2 * np.eye(len(self.X))
            return 0.5 * self.Y.T.dot(inv(K).dot(self.Y)) + \
                np.sum(np.log(np.diagonal(cholesky(K)))) + 0.5 * len(self.X) * np.log(2*np.pi)

        res = minimize(nll, self.kernel.parameters, bounds=self.kernel.bounds, method='L-BFGS-B') 

        # l_opt, sigma_f_opt = res.x
        self.kernel.parameters = res.x
        self.mu, self.sigma = self.predict(self.X, cov=True)

    def predict(self, X, std=False, cov=False):
        ''' This method predict new distribution and provide new sigma '''
        K = self.kernel(self.X, self.X) + self.noise**2 * np.eye(self.X.shape[0])
        K_s = self.kernel(self.X, X)
        K_ss = self.kernel(X, X) + self.noise*np.eye(len(X))
        K_inv = inv(K)

        print('K_s')
        print(K_s)
        mu_s = K_s.T.dot(K_inv).dot(self.Y)

        if std:
            L = cholesky(K)
            print('L')
            print(L)
            Lk = solve(L, K_s)
            print(Lk)
            print(np.diag(K_ss), np.sum(Lk**2, axis=0))
            stdv = np.sqrt(np.diag(K_ss) - np.sum(Lk**2, axis=0))
            if cov:
                cov_s = K_ss - K_s.T.dot(K_inv).dot(K_s)
                return mi_s, stdv, cov_s
            else:
                return mu_s, stdv
        else:
            if cov:
                cov_s = K_ss - K_s.T.dot(K_inv).dot(K_s)
                return mu_s, cov_s
            else:
                return  mu_s

class EI():
    def __init__(self, model, xi=0.01):
        self.model = model
        self.xi = 0.01
    
    def __call__(self, X):
        mu, sigma = self.model.predict(X, std=True)
        mu_sample = self.model.mu
        sigma = sigma.reshape((-1, X.shape[1]))

        mu_sample_opt = np.max(mu_sample) # if noise-free, use max(sample_y)

        with np.errstate(divide='warn'):
            imp = mu - mu_sample_opt - self.xi 
            Z = imp / sigma
            ei = imp * norm.cdf(Z) + sigma * norm.pdf(Z)
            ei[sigma == 0.0] = 0.0
        
        return ei

class Objective():
    def __init__(self):
        pass
    
    def evaluate(self, X):
        raise NotImplementedError()

class Func(Objective):
    def __init__(self, noise=0.2):
        super(Func, self).__init__()
        self.noise = 0.2
    
    def evaluate(self, X):
        return -np.sin(3*X) - X**2 + 0.7*X + self.noise * np.random.randn(*X.shape)

class BO():
    def __init__(self, objective, model, acquisition, bounds, X, Y):
        self.objective = objective
        self.model = model
        self.acquisition = acquisition
        self.bounds = bounds

        self.n_iter = None
        self.n_starts = None

        self.X = X
        self.Y = Y

        self.min_Y = np.min(Y)    
        self.min_X = X[np.argmin(Y)]

    def get_min(self):
        return self.min_X, self.min_Y

    def _propose(self):
        dim = self.X.shape[1]
        min_val = 1
        min_x = None

        def min_obj(X):
            return -self.acquisition(np.atleast_2d(X))

        for x0 in np.random.uniform(self.bounds[:, 0], self.bounds[:, 1], size=(self.n_starts, dim)):
            res = minimize(min_obj, x0=x0, bounds=self.bounds, method='L-BFGS-B')

            if res.fun < min_val:
                min_val = res.fun[0]
                min_x = res.x

        return min_x.reshape((-1, 1))
    
    def run(self, n_iter=10, n_starts=100):
        self.n_iter = n_iter
        self.n_starts = n_starts

        plt.figure(figsize=(12, self.n_iter*3))
        plt.subplots_adjust(hspace=0.4)
        X = np.arange(self.bounds[:, 0], self.bounds[:, 1], 0.01).reshape((-1, 1))
        Y = self.objective.evaluate(X)

        for i in range(self.n_iter):
            self.model.fit(self.X, self.Y)

            X_next = self._propose()
            Y_next = self.objective.evaluate(X_next)

            if Y_next > self.min_Y:
                self.min_Y = Y_next
                self.min_X = X_next

            plt.subplot(self.n_iter, 2, 2*i+1)
            plot_approx(self.model, X, Y, self.X, self.Y, show_legend=(i==0))
            plt.title(f'Iteration {i+1}')
            plt.subplot(self.n_iter, 2, 2*i+2)
            plot_acquisition(X, self.acquisition(X), X_next, show_legend=(i==0))

            self.X = np.vstack((self.X, X_next))
            self.Y = np.vstack((self.Y, Y_next))
    

# Show demos
def show_prior_demo():
    X = np.arange(-5, 5, 0.2).reshape(-1, 1)
    params_K = [1., 1.]
    rbf = RBFKernel(params_K)
    mu = np.zeros(X.shape)
    cov = rbf(X, X)

    samples = np.random.multivariate_normal(mu.ravel(), cov, 3)

    plot_gp(mu, cov, X, samples=samples)
    plt.show()

def show_posterior_demo():
    X_train = np.array([-4, -3, -2, -1, 1]).reshape((-1, 1))
    Y_train = np.sin(X_train)

    X = np.arange(-5, 5, 0.2).reshape(-1, 1)
    
    param_K = [1., 1.]
    rbf = RBFKernel(param_K)

    gpr = GaussianProcess(rbf, X=X_train, Y=Y_train)
    mu_s, cov_s = gpr.predict(X, cov=True)
    samples = np.random.multivariate_normal(mu_s.ravel(), cov_s, 3)

    plot_gp(mu_s, cov_s, X, X_train=X_train, Y_train=Y_train, samples=samples)
    plt.show()

def show_optimizing_demo():
    noise = 0.4
    X_train = np.array([-4, -3, -2, -1, 1]).reshape(-1, 1)
    Y_train = np.sin(X_train) + noise * np.random.randn(*X_train.shape)

    X = np.arange(-5, 5, 0.2).reshape(-1, 1)

    param_K = [1., 1.]
    rbf = RBFKernel(param_K)
    print(rbf.parameters)
    gpr = GaussianProcess(rbf, X=X_train, Y=Y_train)
    mu_s, cov_s = gpr.predict(X, cov=True)
    plot_gp(mu_s, cov_s, X, X_train=X_train, Y_train=Y_train)
    plt.show()

    gpr.fit()
    print(rbf.parameters)
    mu_s, cov_s = gpr.predict(X, cov=True)
    plot_gp(mu_s, cov_s, X, X_train=X_train, Y_train=Y_train)
    plt.show()

def show_bo_demo():
    noise = 0.2
    bounds = np.array([[-1., 2.]])

    objective = Func()
    X_sample = np.array([[-0.9], [1.1]])
    Y_sample = objective.evaluate(X_sample)

    param_K = [1., 1.]
    rbf = RBFKernel(param_K)
    gpr = GaussianProcess(rbf, noise=noise)
    ei = EI(gpr)

    bo = BO(objective, gpr, ei, bounds, X_sample, Y_sample)

    bo.run()

    print(bo.get_min())

    plt.show()

def validation():
    X = np.array([
        [7.8264e-6, 4.8565e-1, 4.7045e-2, 9.3468e-1],
        [1.3154e-1, 5.3277e-1, 6.7886e-1, 3.8350e-1],
        [7.5561e-1, 2.1896e-1, 6.7930e-1, 5.1942e-1]])
    Y = np.array([[0.8310], [0.0346], [0.0535]])
    Xnew = np.array([[0.5297, 0.6711, 0.0077, 0.3834]])
    params_K = [1., 1.]
    rbf = RBFKernel(params_K)
    gpr = GaussianProcess(rbf, X=X, Y=Y)
    mu, stdv = gpr.predict(Xnew, std=True)
    print(mu, stdv)

if __name__ == "__main__":
    # show_prior_demo()
    # show_posterior_demo()
    # show_optimizing_demo()
    # show_bo_demo()
    validation()