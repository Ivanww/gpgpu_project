import numpy as np
import matplotlib.pyplot as plt

def load_ei_log(fn):
    log = []
    with open(fn, 'r') as f:
        for line in f:
            try:
                log.append(float(line.rstrip().lstrip()))
            except:
                pass
    return log

def draw_ei(eis, lb, ub, n=100):
    x = np.linspace(lb, ub, n)
    fig, ax = plt.subplots(10, 1)
    for n, ei in enumerate(eis):
        ax[n].plot(x, ei, label='Iter {}'.format(n))
        ax[n].legend()

if __name__ == "__main__":
    log = [load_ei_log('../cuByGpOpt/build/log_{}.txt'.format(i)) for i in range(10)]
    draw_ei(log, -1, 2.)
    plt.legend()
    plt.grid(True)
    plt.show()
