# This is a prototype demo of GP in python
# Reference here http://krasserm.github.io/2018/03/19/gaussian-processes/
import numpy as np
from numpy.linalg import inv
from numpy.linalg import cholesky
from scipy.optimize import minimize

# some utility function
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

def plot_gp(mu, cov, X, X_train=None, Y_train=None, samples=[]):
    X = X.ravel()
    mu = mu.ravel()
    uncertainty = 1.96 * np.sqrt(np.diag(cov))

    plt.fill_between(X, mu+uncertainty, mu-uncertainty, alpha=0.1)
    plt.plot(X, mu, label='Mean')

    for i, sample in enumerate(samples):
        plt.plot(X, sample, lw=1, ls='--', label=f'Sample {i+1}')
    
    if X_train is not None:
        plt.plot(X_train, Y_train, 'rx')
    
    plt.legend()

# RBF kernel
def kernel(X1, X2, l=1., sigma_f=1.):
    sqdist = np.sum(X1**2, 1).reshape(-1, 1)+np.sum(X2**2, 1) - 2 * np.dot(X1, X2.T)
    return sigma_f**2 * np.exp(-0.5 / l**2 * sqdist)

def posterior_predictive(X_s, X_train, Y_train, l=1., sigma_f=1., sigma_y=1e-8):
    K = kernel(X_train, X_train, l, sigma_f) + sigma_y**2 * np.eye(len(X_train))
    K_s = kernel(X_train, X_s, l, sigma_f)
    K_ss = kernel(X_s, X_s, l, sigma_f) + 1e-8*np.eye(len(X_s))
    K_inv = inv(K)

    mu_s = K_s.T.dot(K_inv).dot(Y_train)

    cov_s = K_ss - K_s.T.dot(K_inv).dot(K_s)

    return mu_s, cov_s

def nll_fn(X_train, Y_train, noise):
    def step(theta):
        K = kernel(X_train, X_train, l=theta[0], sigma_f=theta[1]) + noise**2 * np.eye(len(X_train))
        return np.sum(np.log(np.diagonal(cholesky(K)))) + 0.5 * Y_train.T.dot(inv(K).dot(Y_train)) + \
            0.5 * len(X_train) * np.log(2*np.pi)
    
    return step

# demos
def show_prior_demo():
    X = np.arange(-5, 5, 0.2).reshape(-1, 1)

    mu = np.zeros(X.shape)
    cov = kernel(X, X)

    samples = np.random.multivariate_normal(mu.ravel(), cov, 3)

    plot_gp(mu, cov, X, samples=samples)

def show_posterior_demo():
    X_train = np.array([-4, -3, -2, -1, 1]).reshape(-1, 1)
    Y_train = np.sin(X_train)

    X = np.arange(-5, 5, 0.2).reshape(-1, 1)
    mu_s, cov_s = posterior_predictive(X, X_train, Y_train)
    samples = np.random.multivariate_normal(mu_s.ravel(), cov_s, 3)

    plot_gp(mu_s, cov_s, X, X_train=X_train, Y_train=Y_train, samples=samples)

    plt.show()

def show_noisy_posterior_demo():
    noise = 0.4

    X_train = np.array([-4, -3, -2, -1, 1]).reshape(-1, 1)
    Y_train = np.sin(X_train) + noise * np.random.randn(*X_train.shape)

    X = np.arange(-5, 5, 0.2).reshape(-1, 1)
    mu_s, cov_s = posterior_predictive(X, X_train, Y_train)
    samples = np.random.multivariate_normal(mu_s.ravel(), cov_s, 3)

    plot_gp(mu_s, cov_s, X, X_train=X_train, Y_train=Y_train, samples=samples)

def show_parameters_demo():
    params = [
        (0.3, 1.0, 0.2),
        (3., 1., .2),
        (1., .3, .2),
        (1., 3., .2),
        (1., 1., .05),
        (1., 1., 1.5)
    ]

    noise = 0.4
    X_train = np.array([-4, -3, -2, -1, 1]).reshape(-1, 1)
    Y_train = np.sin(X_train) + noise * np.random.randn(*X_train.shape)

    X = np.arange(-5, 5, 0.2).reshape(-1, 1)

    plt.figure(figsize=(12, 5))

    for i, (l, sigma_f, sigma_y) in enumerate(params):
        mu_s, cov_s = posterior_predictive(X, X_train, Y_train, l, sigma_f, sigma_y)

        plt.subplot(3, 2, i+1)
        plt.subplots_adjust(top=2)
        plt.title(f'l = {l}, sigma_f = {sigma_f}, sigma_y = {sigma_y}')
        plot_gp(mu_s, cov_s, X, X_train=X_train, Y_train=Y_train)
    plt.show()
    
def show_update_parameter():
    noise = 0.4
    X_train = np.array([-4, -3, -2, -1, 1]).reshape(-1, 1)
    Y_train = np.sin(X_train) + noise * np.random.randn(*X_train.shape)

    X = np.arange(-5, 5, 0.2).reshape(-1, 1)
    mu_s, cov_s = posterior_predictive(X, X_train, Y_train)
    print(mu_s.shape, cov_s.shape)
    plot_gp(mu_s, cov_s, X, X_train=X_train, Y_train=Y_train)

    res = minimize(nll_fn(X_train, Y_train, noise), [1, 1], bounds=((1e-5, None), (1e-5, None)),
        method='L-BFGS-B')

    l_opt, sigma_f_opt = res.x
    print(l_opt, sigma_f_opt)
    mu_s, cov_s = posterior_predictive(X, X_train, Y_train, l=l_opt, sigma_f=sigma_f_opt)
    plot_gp(mu_s, cov_s, X, X_train=X_train, Y_train=Y_train)
    # plt.show()
    


if __name__ == "__main__":
    # show_posterior_demo()
    show_update_parameter()