#include <iomanip>
#include <iostream>
#include <vector>
#include <nlopt.hpp>
#include <armadillo>
#include <fstream>

#include "../include/types.h"
#include "../include/kernels.h"
#include "../include/GPR.h"
#include "../include/objective.h"

void check_sigma_prediction(RBFKernel& k, const matrix_t& samples) {
    matrix_t k_mat = k(samples, samples); 
    matrix_t kl_mat = arma::chol(k_mat);
    std::cout << "Kernal Matrix" << std::endl;
    std::cout << k_mat << std::endl;
    std::cout << "Chol Decomposition" << std::endl;
    std::cout << kl_mat << std::endl;
    std::cout << "Reconstruct" << std::endl;
    std::cout << kl_mat.t() * kl_mat << std::endl;

}

void check_h_param_opt(const matrix_t& samples, const vector_t& observations) {
    std_vector_t init_k_params({0.25, 1.});
    RBFKernel k(init_k_params); 
    GPR model(k);
    model.set_samples(samples);
    model.set_observations(observations);
    // BEFORE FITTING
    for (index_t i = 0; i < samples.n_cols; ++i) {
        std::cout << "[Samples]: " << samples.col(i);
        matrix_t mean = model.mu(samples.col(i));
        std::cout << "[Mean Pred]: " << mean(0, 0) << std::endl;
        matrix_t sigma = model.sigma(samples.col(i));
        std::cout << "[Sigma Pred]: " << sigma(0, 0) << std::endl;
    }
    // FITTING MODEL
    model.fit();

    // THEN QUERY
    for (index_t i = 0; i < samples.n_cols; ++i) {
        matrix_t mean = model.mu(samples.col(i));
        matrix_t sigma = model.sigma(samples.col(i));
        
        scala_t m_err = std::abs(mean(0, 0) - observations(i));
        if (m_err < 0.05) {
            std::cout << "Mean prediction check successes! " << std::endl;
            std::cout << "[Mean Prediction Err]: " << m_err << std::endl;
        }
        else {
            std::cout << "Mean prediction check failed! " << std::endl;
            std::cout << "[Mean Prediction Err]: " << m_err << std::endl;
        }

        if (sigma(0, 0) <= 2. * (model.kernel().noise() + 1e-8)) {
            std::cout << "Sigma prediction check successes! " << std::endl;
            std::cout << "[Sigam Prediction]: " << sigma(0, 0) << std::endl;
        }
        else {
            std::cout << "Sigma prediction check failed! " << std::endl;
            std::cout << "[Sigam Prediction]: " << sigma(0, 0) << std::endl;
        }
        std::cout << std::endl;
    }
}

void check_loglik_gradient(const matrix_t& samples, const vector_t& observations, 
    const matrix_t& test_params, scala_t delta) {
    std_vector_t init_k_params({0.25, 1.});
    RBFKernel k(init_k_params);
    GPR model(k);
    model.set_samples(samples);
    model.set_observations(observations);
    scala_t total_err = 0.;
    scala_t total_err_raw = 0.;
    for (index_t i = 0; i < test_params.n_rows; ++i) {
        std_vector_t test_copy(test_params.row(i).begin(), test_params.row(i).end());
        model.kernel().params(test_copy);
        matrix_t analytic = model.gradient_loglik();
        // matrix_t raw_analytic = model.gradient_loglik_raw();
        // numeric
        matrix_t numeric(2, 1);
        test_copy[0] -= delta;
        model.kernel().params(test_copy);
        scala_t ll1 = model.loglik();
        test_copy[0] += 2. * delta;
        model.kernel().params(test_copy);
        scala_t ll2 = model.loglik();
        numeric(0, 0) = (ll2 - ll1) / (2. * delta);

        test_copy[0] -= delta;
        test_copy[1] -= delta;
        model.kernel().params(test_copy);
        scala_t ll3 = model.loglik();
        test_copy[1] += 2*delta;
        model.kernel().params(test_copy);
        scala_t ll4 = model.loglik();
        numeric(1, 0) = (ll4 - ll3) / (2. * delta);

        total_err += arma::norm(analytic - numeric);
        // total_err_raw += arma::norm(analytic - raw_analytic);
    }
    if (total_err < test_params.n_cols * delta) {
        std::cout << "Gradient Check Successes! " << std::endl;
        std::cout << "Total Err: " << total_err << std::endl;
        // std::cout << "Total Raw Err: " << total_err_raw << std::endl;
    }
    else {
        std::cout << "Gradient Check Fail..." << std::endl;
        std::cout << "Total Error on Gradient: " << total_err << std::endl;
    }
}

int main(void) {
    int N = 10; // # of observations
    int M = 100; // # of h_params
    matrix_t samples(1, N, arma::fill::randn);
    samples = samples * 10.;
    std::cout << "Sample range [" << samples.min() << ", " << samples.max() << "]" << std::endl;

    vector_t observations = arma::sin<vector_t>(samples.t());
    
    matrix_t h_params(M, 2, arma::fill::randu);
    h_params = h_params * 2. - 1;
    std::cout << "L parameter range [" << h_params.row(0).min() << ", " << h_params.row(0).max() << "]" << std::endl;
    std::cout << "Sigma_sq parameter range [" << h_params.row(1).min() << ", " << h_params.row(1).max() << "]" << std::endl;

    check_loglik_gradient(samples, observations, h_params, 1e-4);
    std::cout << std::endl;

    matrix_t hp_opt_samples(1, 3, arma::fill::randu);
    hp_opt_samples = hp_opt_samples * 2. - 1;
    Demo demo(0.);

    vector_t hp_opt_obs = demo(hp_opt_samples.t());
    check_h_param_opt(hp_opt_samples, hp_opt_obs);
}