#include "../include/types.h"
#include "../include/kernels.h"
// gradient of X ref: https://github.com/scikit-optimize/scikit-optimize/blob/master/skopt/learning/gaussian_process/kernels.py

void check_kernel_with_gradient(RBFKernel& k, std_vector_t& hp, 
    const matrix_t& x1, const matrix_t& x2, scala_t delta) {
    for (index_t i = 0; i < x2.n_cols; ++i) {
        k.params(hp);
        matrix_t analytic = k.gradient_params(x1, x2.col(i));

        std_vector_t test1(hp);
        test1[0] -= delta;
        k.params(test1);
        matrix_t res1 = k(x1, x2.col(i));
        test1[0] += 2 * delta;
        k.params(test1);
        matrix_t res2 = k(x1, x2.col(i));

        std_vector_t test2(hp);
        test2[1] -= delta;
        matrix_t res3 = k(x1, x2.col(i));
        test2[1] += 2 * delta;
        matrix_t res4 = k(x1, x2.col(i));

        matrix_t l_gradient = (res2 - res1) / (2. * delta);
        matrix_t sigma_gradient = (res4 - res3) / (2. * delta);

        scala_t err_l = arma::norm(l_gradient - analytic.row(0));
        scala_t err_sigma = arma::norm(sigma_gradient - analytic.row(1));
        scala_t total_err = err_l + err_sigma;
        if (total_err > delta) {
            std::cout << "Gradient Check Fail!" << std::endl;
            return;
        }
    }
    std::cout << "Gradient Check Successes! " << std::endl;
}

int main(void) {
    matrix_t h_params(10, 2, arma::fill::randn);
    h_params *= 6;
    h_params -= 3;

    matrix_t x1 = arma::randu<matrix_t>(100, 100) * 10. - 5.;
    matrix_t x2 = arma::randu<matrix_t>(100, 100) * 10. - 5.;

    RBFKernel k;

    for (index_t i = 0; i < 10; ++i) {
        std_vector_t hp(h_params.row(i).begin(), h_params.row(i).end());
        check_kernel_with_gradient(k, hp, x1, x2, 1e-3);
    }
    return 0;
}