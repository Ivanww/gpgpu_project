#include <iomanip>
#include <iostream>
#include <vector>
#include <nlopt.hpp>
#include <armadillo>
#include <fstream>

#include "../include/types.h"
#include "../include/objective.h"
#include "../include/GPR.h"
#include "../include/BO.h"

int check_bo(void) {
    // initial samples
    matrix_t x(1, 2);
    x(0, 0) = -0.9;
    x(0, 1) = 1.1;

    // initial observatons
    Demo demo(0.04);
    vector_t y = demo(x.t());
    std::cout << "X" << std::endl;
    std::cout << x << std::endl;
    std::cout << "Y" << std::endl;
    std::cout << y << std::endl;

    // configure kernel
    std_vector_t params({1., 1.});
    RBFKernel kernel(params);
    kernel.noise(0.04);

    // configure bounds
    std_vector_t lb({-1.});
    std_vector_t ub({2.});

    // build model
    GPR model(kernel);
    model.add_data(x, y);

    // build bo
    BO bo(demo, model, lb, ub);
    bo.run(10, 10);
    
    // std::cout << "Optimization Result: " << 
    return 0;
}

void check_objective(scala_t lb, scala_t ub) {
    Demo demo(0.2);
    matrix_t x = arma::linspace(lb, ub); 
    matrix_t y = demo(x);

    std::ofstream f("objective.txt");
    y.save(f, arma::arma_ascii);
    f.close();
}

int main(void) {
    check_objective(-1, 2.);
    check_bo();
    return 0;
}