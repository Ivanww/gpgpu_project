#include <cuda.h>
#include <cublas_v2.h>
#include <cuda_runtime.h>
#include <cusolverDn.h>
#include <iostream>

#include "../include/types.h"

// static const int THREADS_PER_BLOCK = 128;
extern const int THREADS_PER_BLOCK;

#define CUDA_CALL(x) do { cudaError_t err = x; \
    if ((err)!=cudaSuccess) {   \
    printf("Error at %s:%d: %s\n", __FILE__, __LINE__, cudaGetErrorString(err));\
    return EXIT_FAILURE;}} while(0)

#define CUBLAS_CALL(x) do { if ((x)!=CUBLAS_STATUS_SUCCESS) {\
    printf("Error at %s:%d\n", __FILE__, __LINE__); \
    return EXIT_FAILURE;}} while(0)

#define CUSOLVER_CALL(x) do { if ((x)!=CUSOLVER_STATUS_SUCCESS) {\
    printf("Error at %s:%d\n", __FILE__, __LINE__); \
    return EXIT_FAILURE;}} while(0)

extern __global__ void rbf(scala_t *d_value, scala_t l_sq, scala_t sigma_sq, int N);

extern __global__ void  grad_l(scala_t *d_r, scala_t l_sq, scala_t sigma_sq, int N);

extern __global__ void grad_sigma(scala_t *d_r, scala_t l_sq, scala_t sigma_sq, int N);

extern "C" int __device_radius(scala_t* d_v1, scala_t* d_v2, scala_t *d_r, int N,
    cublasHandle_t handle);

extern "C" int __mixed_noise_vv_rbf(scala_t* d_v1, scala_t* d_v2, scala_t* k, int N, 
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq, cublasHandle_t handle);

extern "C" int __device_vv_rbf(scala_t* d_v1, scala_t* d_v2, scala_t* d_k, int N, 
    scala_t l_sq, scala_t sigma_sq, cublasHandle_t handle);

extern "C" int __device_mm_rbf(scala_t* d_m, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq);

extern "C" int __device_mv_rbf(scala_t* d_m, scala_t* m, scala_t* d_v, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq);

extern "C" int __device_noise_mv_rbf(scala_t* d_m, scala_t* m, scala_t* d_v, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

extern "C" int __device_noise_mm_rbf(scala_t* d_m, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

extern "C" int _device_vv_kernel(const scala_t* v1, const scala_t* v2, scala_t* k,
    int N, scala_t l_sq, scala_t sigma_sq) {
    cublasHandle_t handle;
    scala_t *d_v1, *d_v2;// *d_k;
    // malloc space
    CUDA_CALL(cudaMalloc((void**)&d_v1, N*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_v2, N*sizeof(scala_t)));
    // init handle
    CUBLAS_CALL(cublasCreate(&handle));

    // copy data
    CUDA_CALL(cudaMemcpy(d_v2, v2, N*sizeof(scala_t), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy(d_v1, v1, N*sizeof(scala_t), cudaMemcpyHostToDevice));

    // compute kernel
    scala_t a = -1.;
    CUBLAS_CALL(cublasDaxpy(handle, N, &a, d_v2, 1, d_v1, 1)); // x1 = -x2 + x1
    CUBLAS_CALL(cublasDnrm2(handle, N, d_v1, 1, k));
    (*k) = sigma_sq * exp(-0.5 * (*k) * (*k) / l_sq); // this is a CPU operation

    // clean up
    CUBLAS_CALL(cublasDestroy(handle));
    CUDA_CALL(cudaFree(d_v1));
    CUDA_CALL(cudaFree(d_v2));

    return EXIT_SUCCESS;
}
extern "C" int _device_noise_vv_kernel(const scala_t* v1, const scala_t* v2, scala_t* k,
    int N, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    cublasHandle_t handle;
    scala_t *d_v1, *d_v2;// *d_k;
    // malloc space
    CUDA_CALL(cudaMalloc((void**)&d_v1, N*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_v2, N*sizeof(scala_t)));
    // init handle
    CUBLAS_CALL(cublasCreate(&handle));

    // copy data
    CUDA_CALL(cudaMemcpy(d_v2, v2, N*sizeof(scala_t), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy(d_v1, v1, N*sizeof(scala_t), cudaMemcpyHostToDevice));

    // compute kernel
    // scala_t a = -1.;
    // CUBLAS_CALL(cublasDaxpy(handle, N, &a, d_v2, 1, d_v1, 1)); // x1 = -x2 + x1
    // CUBLAS_CALL(cublasDnrm2(handle, N, d_v1, 1, k));
    // (*k) = sigma_sq * exp(-0.5 * (*k) * (*k) / l_sq); // this is a CPU operation
    __mixed_noise_vv_rbf(d_v1, d_v2, k, N, l_sq, sigma_sq, noise_sq, handle);

    // clean up
    CUBLAS_CALL(cublasDestroy(handle));
    CUDA_CALL(cudaFree(d_v1));
    CUDA_CALL(cudaFree(d_v2));

    return EXIT_SUCCESS;
}
extern "C" int _device_mv_kernel(const scala_t* m1, const scala_t* v2, scala_t* k,
    int row, int col, scala_t l_sq, scala_t sigma_sq) {
    scala_t *d_m1, *d_v2, *d_k;
    // copy v2 in default stream 
    CUDA_CALL(cudaMalloc((void**)&d_v2, row*sizeof(scala_t)));
    CUDA_CALL(cudaMemcpy(d_v2, v2, row*sizeof(scala_t), cudaMemcpyHostToDevice));

    // malloc device space
    CUDA_CALL(cudaMalloc((void**)&d_m1, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_k, col*sizeof(scala_t)));

    // compute kernel
    __device_mv_rbf(d_m1, const_cast<scala_t*>(m1), d_v2, d_k, row, col, l_sq, sigma_sq);

    // copy data
    CUDA_CALL(cudaMemcpy(k, d_k, col*sizeof(scala_t), cudaMemcpyDeviceToHost));

    CUDA_CALL(cudaFree(d_m1));
    CUDA_CALL(cudaFree(d_v2));
    CUDA_CALL(cudaFree(d_k));

    return EXIT_SUCCESS;
}

extern "C" int _device_noise_mv_kernel(const scala_t* m1, const scala_t* v2, scala_t* k,
    int row, int col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    scala_t *d_m1, *d_v2, *d_k;
    // copy v2 in default stream 
    CUDA_CALL(cudaMalloc((void**)&d_v2, row*sizeof(scala_t)));
    CUDA_CALL(cudaMemcpy(d_v2, v2, row*sizeof(scala_t), cudaMemcpyHostToDevice));

    // malloc device space
    CUDA_CALL(cudaMalloc((void**)&d_m1, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_k, col*sizeof(scala_t)));

    // compute kernel
    __device_noise_mv_rbf(d_m1, const_cast<scala_t*>(m1), d_v2, d_k, row, col, l_sq, sigma_sq, noise_sq);

    // copy data
    CUDA_CALL(cudaMemcpy(k, d_k, col*sizeof(scala_t), cudaMemcpyDeviceToHost));

    CUDA_CALL(cudaFree(d_m1));
    CUDA_CALL(cudaFree(d_v2));
    CUDA_CALL(cudaFree(d_k));

    return EXIT_SUCCESS;
}

extern "C" int _device_mm_kernel(const scala_t* m, scala_t* k, int row, int col, 
    scala_t l_sq, scala_t sigma_sq) {
    scala_t *d_m, *d_k;

    CUDA_CALL(cudaMalloc((void**)&d_m, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_k, col*col*sizeof(scala_t)));
    CUDA_CALL(cudaMemcpy(d_m, m, row*col*sizeof(scala_t), cudaMemcpyHostToDevice));
    
   __device_mm_rbf(d_m, d_k, row, col, l_sq, sigma_sq); 

    CUDA_CALL(cudaMemcpy(k, d_k, col*col*sizeof(scala_t), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaFree(d_m));
    CUDA_CALL(cudaFree(d_k));
    return EXIT_SUCCESS;
}

extern "C" int _device_noise_mm_kernel(const scala_t* m, scala_t* k, int row, int col, 
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    scala_t *d_m, *d_k;

    CUDA_CALL(cudaMalloc((void**)&d_m, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_k, col*col*sizeof(scala_t)));
    CUDA_CALL(cudaMemcpy(d_m, m, row*col*sizeof(scala_t), cudaMemcpyHostToDevice));
    
   __device_noise_mm_rbf(d_m, d_k, row, col, l_sq, sigma_sq, noise_sq); 

    CUDA_CALL(cudaMemcpy(k, d_k, col*col*sizeof(scala_t), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaFree(d_m));
    CUDA_CALL(cudaFree(d_k));
    return EXIT_SUCCESS;
}

extern "C" int inv_kernel(const scala_t* m, scala_t* s, int N) {
    // s is aux identity matrix
    scala_t *d_m, *d_s, *ws;
    int Lwork, *info;
    cusolverDnHandle_t handle;
    cudaStream_t fac_s, sol_s;
    cublasFillMode_t lo = CUBLAS_FILL_MODE_LOWER;

    CUDA_CALL(cudaStreamCreate(&fac_s));
    CUDA_CALL(cudaStreamCreate(&sol_s));
    CUSOLVER_CALL(cusolverDnCreate(&handle));

    CUDA_CALL(cudaMalloc((void**)&d_m, N*N*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_s, N*N*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&info, sizeof(int)));

    CUDA_CALL(cudaHostRegister((void*)m, N*N*sizeof(scala_t), cudaHostRegisterDefault));
    CUDA_CALL(cudaHostRegister((void*)s, N*N*sizeof(scala_t), cudaHostRegisterDefault));
    // copy matrix data
    CUDA_CALL(cudaMemcpyAsync(d_m, m, N*N*sizeof(scala_t), cudaMemcpyHostToDevice, fac_s));
    // compute workspace size and malloc workspace
    CUSOLVER_CALL(cusolverDnSetStream(handle, fac_s));
    CUSOLVER_CALL(cusolverDnDpotrf_bufferSize(handle, lo, N, d_m, N, &Lwork));
    CUDA_CALL(cudaMalloc((void **)&ws, Lwork*sizeof(scala_t)));
    // decompose
    CUSOLVER_CALL(cusolverDnDpotrf(handle, lo, N, d_m, N, ws, Lwork, info));

    // copy identity matrix
    CUSOLVER_CALL(cusolverDnSetStream(handle, sol_s));
    CUDA_CALL(cudaMemcpyAsync(d_s, s, N*N*sizeof(scala_t), cudaMemcpyHostToDevice, sol_s));
    CUDA_CALL(cudaDeviceSynchronize());
    CUSOLVER_CALL(cusolverDnDpotrs(handle, lo, N, N, d_m, N, d_s, N, info));

    CUDA_CALL(cudaDeviceSynchronize());
    // copy out
    CUDA_CALL(cudaMemcpy(s, d_s, N*N*sizeof(scala_t), cudaMemcpyDeviceToHost));

    // clean up
    CUDA_CALL(cudaHostUnregister((void*)m));
    CUDA_CALL(cudaHostUnregister((void*)s));
    CUDA_CALL(cudaFree(d_m));
    CUDA_CALL(cudaFree(d_s));
    CUDA_CALL(cudaFree(ws));
    CUDA_CALL(cudaFree(info));
    CUSOLVER_CALL(cusolverDnDestroy(handle));
    CUDA_CALL(cudaDeviceReset());
    return 0;
}

extern "C" int grad_kernel(const scala_t* m1, const scala_t * v2, scala_t* grad, 
    int row, int col, scala_t l_sq, scala_t sigma_sq) {
    scala_t *d_m1, *d_v2, *d_grad;
    cudaStream_t *streams = (cudaStream_t *)malloc(col*sizeof(cudaStream_t));
    cudaStream_t dl_s, dsigma_s;
    cublasHandle_t handle;
    // copy v2 in default stream 
    CUDA_CALL(cudaMalloc((void**)&d_v2, row*sizeof(scala_t)));
    CUDA_CALL(cudaMemcpy(d_v2, v2, row*sizeof(scala_t), cudaMemcpyHostToDevice));
    CUBLAS_CALL(cublasCreate(&handle));

    // launch streams
    for (int i = 0; i < col; ++i) {
        CUDA_CALL(cudaStreamCreate(&streams[i]));
    }
    CUDA_CALL(cudaStreamCreate(&dl_s));
    CUDA_CALL(cudaStreamCreate(&dsigma_s));
    // malloc matrix space and grad space 
    CUDA_CALL(cudaMalloc((void**)&d_m1, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_grad, 2*col*sizeof(scala_t)));
    
    CUDA_CALL(cudaHostRegister((void*)m1, row*col*sizeof(scala_t), cudaHostRegisterDefault));
    // copy data and compute radius 
    for (int i = 0; i < col; ++i) {
        CUBLAS_CALL(cublasSetStream(handle, streams[i]));
        CUDA_CALL(cudaMemcpyAsync(d_m1+i*row, m1+i*row, row*sizeof(scala_t), cudaMemcpyHostToDevice, streams[i]));
        __device_radius(d_m1+i*row, d_v2, d_grad+i, row, handle);
    }
    // copy radius and calculate gradients
    CUDA_CALL(cudaDeviceSynchronize());
    CUDA_CALL(cudaMemcpy(d_grad+col, d_grad, col*sizeof(scala_t), cudaMemcpyDeviceToDevice));

    int BLOCK = (col+THREADS_PER_BLOCK-1) / THREADS_PER_BLOCK;
    grad_l<<<BLOCK, THREADS_PER_BLOCK, 0, dl_s>>>(d_grad, l_sq, sigma_sq, col);
    grad_sigma<<<BLOCK, THREADS_PER_BLOCK, 0, dsigma_s>>>(d_grad+col, l_sq, sigma_sq, col);

    // copy out
    CUDA_CALL(cudaDeviceSynchronize());
    CUDA_CALL(cudaMemcpy(grad, d_grad, 2*col*sizeof(scala_t), cudaMemcpyDeviceToHost));
    // CUDA_CALL(cudaMemcpy2D((void*)grad, sizeof(scala_t), (void*)d_grad, sizeof(scala_t), col*sizeof(scala_t), 2, cudaMemcpyDeviceToHost));
    // clean up
    for (int i = 0; i < col; ++i) {
        CUDA_CALL(cudaStreamDestroy(streams[i]));
    }
    CUDA_CALL(cudaStreamDestroy(dl_s));
    CUDA_CALL(cudaStreamDestroy(dsigma_s));
    CUBLAS_CALL(cublasDestroy(handle));
    CUDA_CALL(cudaHostUnregister((void*)m1));
    CUDA_CALL(cudaFree(d_m1));
    CUDA_CALL(cudaFree(d_v2));
    CUDA_CALL(cudaFree(d_grad));
    free(streams);

    return 0;
}