#include "../include/types.h"
#include "../include/GPR.h"
#include "CudaLBFGS/lbfgs.h"
#include "CudaLBFGS/cost_function.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <iostream>
#include <armadillo>

class gpr_opt : public cost_function {
public:
    gpr_opt(GPR& m) : cost_function(2), mModel(m) {}

    void f(const float *dx, float*df) {

    }

    void gradf(const float *dX, float *dgrad) {

    }

    void f_gradf(const float *dx, float *df, float *dgrad) {
        f(dx, df);
        gradf(dx, dgrad);
    }
private:
    GPR& mModel;
    
};
int main(void) {
    std::cout << "Interact with arma pointers" << std::endl;
    matrix_t x(3, 4, arma::fill::randn);
    vector_t y(3, 1, arma::fill::randn);
    std::cout << "X data" << std::endl;
    std::cout << x << std::endl;
    std::cout << "Y data" << std::endl;
    std::cout << y << std::endl;
    GPR model(x, y);

    gpr_opt opt(model);
    // index_t N = a.n_elem;
    // scala_t *p = a.memptr();

    std::cout << "Test of GPR fit" << std::endl;
    
    return 0;
}