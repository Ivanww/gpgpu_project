#include "../include/types.h"
#include "../include/kernels.h"
#include <iostream>
#include <limits>
#include <cmath>

extern "C" int _device_vv_kernel(const scala_t* v1, const scala_t* v2, scala_t* k, int N,
    scala_t l_sq, scala_t sigma_sq);

extern "C" int _device_noise_vv_kernel(const scala_t* v1, const scala_t* v2, scala_t* k, int N,
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

int check_cu_kernel_vv() {
    // generate 2 vector
    int M = 10000;
    std_vector_t params({1, 0.25}); // l, sigma_sq
    vector_t x1(M, arma::fill::randu);
    vector_t x2(M, arma::fill::randu);
    matrix_t device_kern(1, 1);
    RBFKernel k(params);

    // compute on host
    matrix_t host_kern = k(x1, x2);
    // compute on device
    // _device_vv_kernel(x1.memptr(), x2.memptr(), device_kern.memptr(), M, params[0]*params[0], params[1]);
    _device_noise_vv_kernel(x1.memptr(), x2.memptr(), device_kern.memptr(), M, params[0]*params[0], params[1], 1e-2);
    scala_t err = host_kern(0, 0) - device_kern(0, 0);
    
    std::cout << "Host Kern: " << host_kern(0, 0) << std::endl;
    std::cout << "Device Kern: " << device_kern(0, 0) << std::endl;
    std::cout << "Kern Error: " << err << std::endl;
    if (std::abs(host_kern(0, 0) - device_kern(0, 0)) < 1e-8) {
        std::cout << "Vector-Vector kernel check passed!" << std::endl;
    }
    else {
        std::cout << "Vector-Vector kernel check failed!" << std::endl;
    }

    return 0;
}

extern "C" int _device_mv_kernel(const scala_t* m1, const scala_t* v2, scala_t* k,
    int row, int col, scala_t l_sq, scala_t sigma_sq);

extern "C" int _device_noise_mv_kernel(const scala_t* m1, const scala_t* v2, scala_t* k,
    int row, int col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

int check_cu_kernel_mv() {
    int M = 10;
    int N = 8;
    std_vector_t params({1, 0.25}); // l, sigma_sq
    matrix_t x1(M, N, arma::fill::randu);
    vector_t x2(M, arma::fill::randu);
    matrix_t device_kern(1, N);
    matrix_t device_noise_kern(1, N);
    RBFKernel k(params);

    // compute on host
    matrix_t host_kern = k(x1, x2);
    _device_noise_mv_kernel(x1.memptr(), x2.memptr(), device_noise_kern.memptr(), 
        M, N, params[0]*params[0], params[1], k.noise());
    std::cout << "Host Kern: " << host_kern << std::endl;
    // std::cout << "Device Kern: " << device_kern << std::endl;
    // std::cout << "Host Noise Kern: " << host_noise_kern << std::endl;
    std::cout << "Device Noise Kern: " << device_noise_kern << std::endl;
    // if (arma::abs(host_kern-device_kern).max() < 1e-8) {
    if (arma::abs(host_kern-device_noise_kern).max() < 1e-8) {
        std::cout << "Matrix-Vector kernel check passed!" << std::endl;
    }
    else {
        std::cout << "Matrix-Vector kernel check failed!" << std::endl;
    }
    return 0;
}

extern "C" int _device_mm_kernel(const scala_t* m, scala_t* k, int row, int col, 
    scala_t l_sq, scala_t sigma_sq);
extern "C" int _device_noise_mm_kernel(const scala_t* m, scala_t* k, int row, int col, 
    scala_t l_sq, scala_t sigma_sq, scala_t noise);

int check_cu_kernel_mm() {
    int M = 1000;
    int N = 8;
    std_vector_t params({1, 0.25}); // l, sigma_sq
    matrix_t x1(M, N, arma::fill::randu);
    matrix_t device_kern(N, N, arma::fill::zeros);
    matrix_t device_noise_kern(N, N, arma::fill::zeros);
    RBFKernel k(params);

    matrix_t host_kern = k(x1, x1);
    // _device_mm_kernel(x1.memptr(), device_kern.memptr(), M, N, params[0]*params[0], params[1]);
    _device_noise_mm_kernel(x1.memptr(), device_noise_kern.memptr(), M, N, params[0]*params[0], params[1], 1e-2);
    std::cout << host_kern << std::endl;
    // std::cout << device_kern << std::endl;
    std::cout << device_noise_kern << std::endl;
    if (arma::abs(host_kern-device_noise_kern).max() < 1e-8) {
        std::cout << "Matrix-Matrix kernel check passed!" << std::endl;
    }
    else {
        std::cout << "Matrix-Matrix kernel check failed!" << std::endl;
    }
    return 0;
}

extern "C" int inv_kernel(const scala_t* m, scala_t* s, int N);

int check_inverse_matrix() {
    int M = 100;
    int N = 8;
    std_vector_t params({1, 0.25}); // l, sigma_sq
    matrix_t x1(M, N, arma::fill::randu);
    matrix_t device_inv(N, N, arma::fill::eye);
    RBFKernel k(params);   
    matrix_t k_mat = k.kernel(x1, x1);
    matrix_t host_inv = k_mat.i();
    std::cout << "Host inverse:" << std::endl;
    std::cout << host_inv << std::endl;
    inv_kernel(k_mat.memptr(), device_inv.memptr(), N);
    std::cout << "Device inverse:" << std::endl;
    std::cout << device_inv << std::endl;
    if (arma::abs(host_inv-device_inv).max() < 1e-8) {
        std::cout << "Kernel inverse check passed!" << std::endl;
    }
    else {
        std::cout << "Kernel inverse check failed!" << std::endl;
    }
    return 0;
}

extern "C" int grad_kernel(const scala_t* m1, const scala_t* v2, scala_t* grad,
    int row, int col, scala_t l_sq, scala_t sigma_sq);

int check_kernel_gradients() {
    int M = 100;
    int N = 8;
    std_vector_t params({1, 0.25}); // l, sigma_sq
    matrix_t x1(M, N, arma::fill::randu);
    RBFKernel k(params);   
    vector_t v2(M, arma::fill::randu);
    matrix_t host_grad = k.gradient_params(x1, v2);
    matrix_t device_grad(x1.n_cols, 2, arma::fill::zeros);
    grad_kernel(x1.memptr(), v2.memptr(), device_grad.memptr(), M, N, params[0]*params[0], params[1]);

    std::cout << "Host gradients" << std::endl;
    std::cout << host_grad << std::endl;
    std::cout << "Device gradients" << std::endl;
    std::cout << device_grad.t() << std::endl;

    if (arma::abs(host_grad-device_grad.t()).max() < 1e-8) {
        std::cout << "Kernel gradient check passed!" << std::endl;
    }
    else {
        std::cout << "Kernel gradient check failed!" << std::endl;
    }
    return 0;
}
int main(void) {
    // check_cu_kernel_vv();
    check_cu_kernel_mv();
    // check_cu_kernel_mm();
    // check_inverse_matrix();
    // check_kernel_gradients();
    return 0;
}