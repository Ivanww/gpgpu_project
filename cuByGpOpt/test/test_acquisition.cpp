#include "../include/types.h"
#include "../include/GPR.h"
#include "../include/acquisition.h"
#include "../include/objective.h"
#include <iostream>
#include <fstream>
#include <armadillo>
#include <cmath>
#include <nlopt.hpp>
#include <string>
#include <sstream>

// helper function
void plot_ei(scala_t start, scala_t end, EI& ei, const std::string& dump) {
    vector_t x = arma::linspace<vector_t>(start, end);
    vector_t eis(x.n_rows);
    for (index_t i = 0; i < x.n_rows; ++i) {
        eis(i) = ei(x.row(i));
    }
    std::ofstream f(dump);
    eis.save(f, arma::arma_ascii);
}

std_vector_t ei_opt_iteration(GPR& model, EI& ei, std_vector_t& lb, std_vector_t& ub, const std::string& logfile) {
    // check fitting result
    model.fit();
    matrix_t samples = model.samples();
    for (index_t i = 0; i < samples.n_rows; ++i) {
        matrix_t mean = model.mu(samples.col(i));
        matrix_t sigma = model.sigma(samples.col(i));
        std::cout << "[GPR] sample " << samples(i, 0) << std::endl;
        std::cout << "[GPR] prediction mu " << mean(0, 0) << std::endl;
        std::cout << "[GPR] prediction sigma_sq " << sigma(0, 0) << std::endl;
    }
    plot_ei(-1., 2., ei, logfile);

    //
    matrix_t tester(1, 3, arma::fill::randu);
    for (index_t i = 0; i < tester.n_rows; ++i) {
        scala_t scale = ub[i] - lb[i];
        tester.row(i) = tester.row(i) * scale + lb[i];
    }
    ACQobj acqopt(ei);
    nlopt::opt opt(nlopt::GN_DIRECT_L_RAND, 1);
    opt.set_lower_bounds(lb);
    opt.set_upper_bounds(ub);
    opt.set_maxeval(80);
    std_vector_t proposed(1);
    scala_t gmaxf = -arma::datum::inf;
    opt.set_max_objective(ACQobj::wrap, &acqopt);
    for (int i = 0; i < tester.n_cols; ++i) {
        std_vector_t init(tester.col(i).begin(), tester.col(i).end());
        double maxf;
        try {
            opt.optimize(init, maxf);
        }
        catch (std::exception& e) {
            std::cout << "[NLOPT] " << e.what() << std::endl;
        }
        std::cout << "[EI] max ei: " << maxf << std::endl;
        std::cout << "[EI] maximal location: " << init[0] << std::endl;
        if (maxf > gmaxf) {
            proposed = init;
        }
    }
    return proposed;
}

void check_ei() {
    matrix_t samples(1, 2);
    samples(0, 0) = -0.9;
    samples(0, 1) = 1.1;

    Demo demo(0.04);
    vector_t observations = demo(samples.t());
    std::cout << "X" << std::endl;
    std::cout << samples << std::endl;
    std::cout << "Y" << std::endl;
    std::cout << observations << std::endl;

    // configure kernel
    std_vector_t params({1., 1.});
    RBFKernel kernel(params);
    kernel.noise(0.04);

    // configure bounds
    std_vector_t lb({-1.});
    std_vector_t ub({2.});

    // build model
    GPR model(kernel);
    model.set_samples(samples);
    model.set_observations(observations);
    EI ei(model);
    for (int i = 0; i < 10; ++i) {
        std::stringstream ss;
        ss << "log_" << i << ".txt";
        std_vector_t next = ei_opt_iteration(model, ei, lb, ub, ss.str());

        matrix_t newob = demo(next);
        model.add_data(next, newob.t());
    }
}

scala_t check_foo(const std_vector_t& x, std_vector_t& grad, void* fdata) {
    if (!grad.empty()) {
        grad[0] = 2. * x[0];
        grad[1] = 2. * x[1];
    }
    return (std::pow(x[0], 2.) + std::pow(x[1], 2.));
}

void check_nlopt_nograd() {
    nlopt::opt opt(nlopt::GN_DIRECT_L_RAND, 2);
    opt.set_min_objective(check_foo, NULL);
    opt.set_maxeval(80);
    row_t init_row(2, arma::fill::randu);
    std_vector_t init_x(init_row.begin(), init_row.end());
    std_vector_t lb({-1, -1});
    std_vector_t ub({1, 1});
    opt.set_lower_bounds(lb);
    opt.set_upper_bounds(ub);

    scala_t minf;
    std::cout << "Starting point (" << init_x[0] << " " 
        << init_x[1] << ")" <<  std::endl;
    try {
        opt.optimize(init_x, minf);
    }
    catch (std::exception& e) {
        std::cout << "[NLOPT] " << e.what() << std::endl;
    }
    std::cout << "Finishing point (" << init_x[0] << " " 
        << init_x[1] << ")" <<  std::endl;
    scala_t err = std::abs(init_x[0]);
    err += std::abs(init_x[1]);

    std::cout << "Optimization finisehed with error " << err << std::endl;
}

void check_nlopt_grad() {
    nlopt::opt opt(nlopt::LD_LBFGS, 2);
    opt.set_min_objective(check_foo, NULL);
    opt.set_maxeval(80);
    row_t init_row(2, arma::fill::randu);
    std_vector_t init_x(init_row.begin(), init_row.end());
    std_vector_t lb({-1, -1});
    std_vector_t ub({1, 1});
    opt.set_lower_bounds(lb);
    opt.set_upper_bounds(ub);

    scala_t minf;
    std::cout << "Starting point (" << init_x[0] << " " 
        << init_x[1] << ")" <<  std::endl;
    try {
        opt.optimize(init_x, minf);
    }
    catch (std::exception& e) {
        std::cout << "[NLOPT] " << e.what() << std::endl;
    }
    std::cout << "Finishing point (" << init_x[0] << " " 
        << init_x[1] << ")" <<  std::endl;
    scala_t err = std::abs(init_x[0]);
    err += std::abs(init_x[1]);

    std::cout << "Optimization finisehed with error " << err << std::endl;

}

void check_pdf_arma() {
    vector_t x(100, arma::fill::randu);
    vector_t pdf = arma::normpdf(x);
    scala_t maxerr = 0.;
    for (index_t i = 0; i < x.n_rows; ++i) {
        scala_t mpdf = std::exp(-0.5 * std::pow(x(i), 2.)) / std::sqrt(2. * M_PI);
        if (maxerr < std::abs(mpdf-pdf(i))) {
            maxerr = std::abs(mpdf-pdf(i));
        }
    }
    std::cout << "Max Err of PDF computation: " << maxerr << std::endl;
}

void check_cdf_arma() {
    vector_t x(100, arma::fill::randu);
    vector_t cdf = arma::normcdf(x);
    scala_t maxerr = 0.;
    for (index_t i = 0; i < x.n_rows; ++i) {
        scala_t mcdf = 0.5 * std::erfc(-x(i)/std::sqrt(2));
        if (maxerr < std::abs(mcdf-cdf(i))) {
            maxerr = std::abs(mcdf-cdf(i));
        }
    }
    std::cout << "Max Err of CDF computation: " << maxerr << std::endl;
}
int main(void) {
    check_pdf_arma();
    check_cdf_arma();
    check_nlopt_grad();
    check_nlopt_nograd();
    check_ei();
    return 0;
}