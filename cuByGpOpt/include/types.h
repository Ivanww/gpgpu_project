#ifndef __TYPES_H__
#define __TYPES_H__

#include <vector>
#include <armadillo>

typedef double scala_t; // use double to cooperate with optimizer
typedef unsigned int index_t;
typedef std::vector<scala_t> std_vector_t;
typedef arma::Row<scala_t> row_t;
typedef arma::Col<scala_t> vector_t;
typedef arma::Mat<scala_t> matrix_t;

static const int THREADS_PER_BLOCK = 128;
#endif