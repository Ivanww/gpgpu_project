#ifndef __OBJECTIVE_H__
#define __OBJECTIVE_H__
#include <random>
#include "types.h"

class Objective {
public:
    Objective(scala_t noise) : m_noise(noise) {}
    virtual ~Objective() {}

    matrix_t operator()(const matrix_t& x) {
        matrix_t noisefree = evaluate(x);
        matrix_t noise(arma::size(noisefree), arma::fill::randn);
        return noisefree + m_noise * noise;
    }
    virtual matrix_t evaluate(const matrix_t& x) = 0;

    scala_t noise() { return m_noise; }
    void noise(scala_t noise) { m_noise = noise; }

protected:
    scala_t m_noise;
    
};

class Demo : public Objective {
public:
    Demo(scala_t noise) : Objective(noise) {}
    ~Demo() {}

    matrix_t evaluate(const matrix_t& x) {
        return -arma::sin(3*x) - arma::pow(x, 2.) + 0.7*x;
    }
};

#endif