#ifndef __BO_H__
#define __BO_H__
#include "types.h"
#include "GPR.h"
#include "objective.h"
#include "acquisition.h"


class BO {
public:
    BO(Objective& obj, GPR& model) : m_objective(obj), m_model(model), m_ei(model) { }
    BO(Objective& obj, GPR& model, const std_vector_t& lb, const std_vector_t& ub) :
        m_objective(obj), m_model(model), m_ei(model), m_lower_bounds(lb), m_upper_bounds(ub) { }
    ~BO () {}


    Objective& obj() { return m_objective; }
    GPR& model() { return m_model; }
    EI& acq() { return m_ei; }

    std_vector_t lower_bounds() { return m_lower_bounds; }
    std_vector_t upper_bounds() { return m_upper_bounds; }
    void lower_bounds(const std_vector_t b) { 
        if (m_lower_bounds.empty()) {
            m_lower_bounds = b;
        }     
    }
    void upper_bounds(const std_vector_t b) { 
        if (m_upper_bounds.empty()) {
            m_upper_bounds = b;
        }
     }

    void run(int n_iter, int n_starts);
    scala_t minf() { return m_model.min_observation(); }
    // matrix_t minx() { }
    EI& acquisition() { return m_ei; }

private:
    Objective& m_objective;
    GPR& m_model;
    EI m_ei;
    std_vector_t m_lower_bounds;
    std_vector_t m_upper_bounds;

    row_t propose(int n_starts);
};
#endif