#ifndef __KERNELS_H__ 
#define __KERNELS_H__
#include <vector>
#include "types.h"

class RBFKernel {
public:
    RBFKernel();
    RBFKernel(const std_vector_t& params);
    ~RBFKernel();

    // matrix_t operator() (const row_t& v1, const row_t& v2);
    matrix_t operator() (const matrix_t& x1, const matrix_t& x2);
    matrix_t kernel(const matrix_t& x1, const matrix_t& x2);
    matrix_t cu_kernel(const matrix_t& x1, const matrix_t& x2);
    matrix_t gradient_params(const matrix_t& x1, const vector_t& x2);
    matrix_t cu_gradient_params(const matrix_t& x1, const vector_t& x2);
    matrix_t gradient_sigma(const matrix_t& x1, const matrix_t& x2);
    matrix_t gradient_l(const matrix_t& x1, const matrix_t& x2);

    // matrix_t gradient_x(const matrix_t&, const matrix_t&);
    // matrix_t gradient_x(const matrix_t&, const matrix_t&, scala_t, scala_t);

    scala_t noise() { return m_noise; }
    void noise(scala_t n) { m_noise = n; }
    scala_t sigma() {return m_sigma_sq; }
    void sigma(scala_t sigma) { m_sigma_sq = sigma; }
    scala_t l() { return m_l; }
    void l(scala_t l) { m_l = l; }
    std_vector_t params() { return std_vector_t({m_l, m_sigma_sq}); }
    void params(const std_vector_t& params);


private:
    scala_t m_l;
    scala_t m_sigma_sq;
    scala_t m_noise;
    matrix_t m_h_params;

    // NOT IMPLEMENTED
    RBFKernel(const RBFKernel&);
    void operator=(const RBFKernel&);
};

#endif