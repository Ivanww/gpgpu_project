#ifndef __GAUSSIAN_PROC_R_H__
#define __GAUSSIAN_PROC_R_H__

#include <vector>
#include "types.h"
#include "kernels.h"

class GPR;

class Nllobj {
public:
    static scala_t wrap(const std_vector_t& x, std_vector_t& grad, void *data) {
        return (*reinterpret_cast<Nllobj*>(data))(x, grad);
    }

    Nllobj(GPR&);
    ~Nllobj();
    scala_t operator()(const std_vector_t&, std_vector_t&);
private:
    GPR& mGPR;
};

class GPR {
public:

    GPR();
    GPR(RBFKernel& kernel);
    GPR(RBFKernel& kernel, std_vector_t& bounds);
    ~GPR();

    void fit();
    void fit(const matrix_t& x, const vector_t& y);
    
    // predictions
    matrix_t mu(const matrix_t& vs);
    matrix_t cu_mu(const matrix_t& vs);
    matrix_t sigma(const matrix_t& vs);
    matrix_t cu_sigma(const matrix_t& vs);

    scala_t loglik();
    scala_t cu_loglik();
    matrix_t gradient_loglik();
    matrix_t cu_gradient_loglik();
    matrix_t gradient_loglik_raw();
    matrix_t gradient_loglik_numeric();

    scala_t min_observation();
    scala_t max_observation();

    RBFKernel& kernel() { return m_kern; }
    matrix_t samples() { return m_samples; }
    void set_samples(const matrix_t& s) { m_samples = s; }
    vector_t observations() { return m_observations; }
    void set_observations(const vector_t& obs) { m_observations = obs; }
    int observation_dim() { return 1; }
    int sample_dim() { return m_samples.n_rows; }

    void add_data(const matrix_t& n_samples, const vector_t& n_observatons);

private:
    RBFKernel m_kern;

    matrix_t m_samples;
    vector_t m_observations;

    // NOT IMPLEMENTED
    GPR(const GPR&);
    void operator=(const GPR&);

};
#endif