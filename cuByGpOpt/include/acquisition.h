#ifndef __ACQUISITION_H__
#define __ACQUISITION_H__
#include "types.h"
#include "GPR.h"

class EI {
public:
    EI(GPR& model);
    EI(GPR& model, scala_t xi);
    ~EI();

    scala_t operator()(const row_t&);
    // matrix_t gradient(const matrix_t&);

    scala_t xi() {return m_xi; }
    void xi(scala_t x) { m_xi = x; }

private:
    GPR& m_model;
    scala_t m_xi;
};

class ACQobj {
public:
    static scala_t wrap(const std_vector_t& x, std_vector_t& grad, void *data) {
        return (*reinterpret_cast<ACQobj*>(data))(x, grad);
    }
    
    ACQobj(EI& ei) : m_ei(ei) {}
    ~ACQobj() {}

    scala_t operator() (const std_vector_t& x, std_vector_t& grad);

private:
    EI& m_ei;
};
#endif