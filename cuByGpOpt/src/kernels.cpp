#include "../include/kernels.h"
#include <cmath>
#include <cassert>

RBFKernel::RBFKernel(): m_l(1.), m_sigma_sq(1.), m_noise(1e-2), m_h_params(1, 2) {
    m_h_params(0, 0) = std::log(m_l);
    m_h_params(0, 1) = std::log(std::sqrt(m_sigma_sq));
}
RBFKernel::RBFKernel(const std_vector_t& params): m_l(params[0]), m_sigma_sq(params[1]), 
    m_noise(1e-2), m_h_params(1, 2) { 
    m_h_params(0, 0) = std::log(m_l);
    m_h_params(0, 1) = std::log(std::sqrt(m_sigma_sq));
    if (params.size() > 2) {
        m_noise = params[2];
    }
}

RBFKernel::~RBFKernel() {}

// expect log
void RBFKernel::params(const std_vector_t& params) {
    m_h_params = params;
    m_l = std::exp(params[0]);
    m_sigma_sq = std::exp(2. * params[1]);
    if (params.size() > 2) {
        m_noise = std::exp(2. * params[2]);
    }
}

matrix_t RBFKernel::operator()(const matrix_t& x1, const matrix_t& x2){
    // return kernel(x1, x2);
    return cu_kernel(x1, x2);
}
matrix_t RBFKernel::kernel(const matrix_t& x1, const matrix_t& x2){
    matrix_t k(x2.n_cols, x1.n_cols);
    scala_t l_sq = m_l * m_l;
    for (index_t i = 0; i < x1.n_cols; ++i) {
        for (index_t j = 0; j < x2.n_cols; ++j) {
            scala_t r = pow(arma::norm(x1.col(i)-x2.col(j)), 2.) / l_sq;
            k(j, i) = m_sigma_sq*exp(-0.5 * r);
        }
    }
    return k + m_noise * arma::eye<matrix_t>(x2.n_cols, x1.n_cols);
}

matrix_t RBFKernel::gradient_params(const matrix_t& x1, const vector_t& x2){
    // matrix_t grads(2, x1.n_cols); // dkdl, dkdsigma
    // scala_t l_sq = m_l * m_l;

    // for (index_t i = 0; i < x1.n_cols; ++i) {
    //         scala_t r = std::pow(arma::norm(x1.col(i)-x2), 2.) / l_sq;
    //         scala_t k = m_sigma_sq * std::exp(-0.5 * r);
    //         grads(0, i) = r * k;
    //         grads(1, i) = 2 * k;
    // }
    // return grads; 
    return cu_gradient_params(x1, x2);
}

matrix_t RBFKernel::gradient_sigma(const matrix_t& x1, const matrix_t& x2) {
    matrix_t grad_sigma(x2.n_cols, x1.n_cols);
    scala_t l_sq = m_l * m_l;
    for (index_t i = 0; i < x1.n_cols; ++i) {
        for (index_t j = 0; j < x2.n_cols; ++j) {
            scala_t r = std::pow(arma::norm(x1.col(i)-x2.col(j)), 2.) / l_sq;
            scala_t k = m_sigma_sq * std::exp(-0.5 * r);
            grad_sigma(j, i) = 2 * k;
        }
    }
    return grad_sigma;
}

matrix_t RBFKernel::gradient_l(const matrix_t& x1, const matrix_t& x2) {
    matrix_t grad_l(x2.n_cols, x1.n_cols);
    scala_t l_sq = m_l * m_l;
    for (index_t i = 0; i < x1.n_cols; ++i) {
        for (index_t j = 0; j < x2.n_cols; ++j) {
            scala_t r = std::pow(arma::norm(x1.col(i)-x2.col(j)), 2.) / l_sq;
            scala_t k = m_sigma_sq * std::exp(-0.5 * r);
            grad_l(j, i) = r * k;
        }
    }
    return grad_l;
}

// compute rbf kernel on GPU
extern "C" int _device_kernel(const scala_t* x1, const scala_t* x2, scala_t* k,
    int x1_col, int row, int x2_col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

matrix_t RBFKernel::cu_kernel(const matrix_t& x1, const matrix_t& x2) {
    matrix_t k(x2.n_cols, x1.n_cols);
    scala_t l_sq = m_l * m_l;
    _device_kernel(x1.memptr(), x2.memptr(), k.memptr(), 
        x1.n_cols, x1.n_rows, x2.n_cols, l_sq, m_sigma_sq, m_noise);
    return k;
}

extern "C" int _device_kernel_gradient(const scala_t* m1, const scala_t * v2, scala_t* t_grad, 
    int row, int col, scala_t l_sq, scala_t sigma_sq);

matrix_t RBFKernel::cu_gradient_params(const matrix_t& x1, const vector_t& x2) {
    matrix_t grads(x1.n_cols, 2);
    scala_t l_sq = m_l * m_l;
    _device_kernel_gradient(x1.memptr(), x2.memptr(), grads.memptr(), 
        x1.n_rows, x1.n_cols, l_sq, m_sigma_sq);
    return grads.t();
}
// matrix_t RBFKernel::gradient_x(const matrix_t& x, const matrix_t& nx) {
//     assert(nx.n_rows == 1);
//     matrix_t grads(x.n_rows, x.n_cols);
//     matrix_t Ks = this->operator()(x, nx);
//     scala_t mL2 = mL * mL;
//     for (int i = 0; i < x.n_rows; ++i) {
//         for (int j = 0; j < x.n_cols; ++j) {
//             grads(i, j) = Ks(i) * (x(i, j) - nx(0, j) / mL2);
//         }
//     }

//     return grads;
// }

// matrix_t RBFKernel::gradient_x(const matrix_t& x, const matrix_t& nx, scala_t sigma, scala_t l) {
//     assert(nx.n_rows == 1);

//     matrix_t grads(x.n_rows, x.n_cols);
//     matrix_t Ks = this->operator()(x, nx, sigma, l);
//     scala_t l2 = l * l;
//     for (int i = 0; i < x.n_rows; ++i) {
//         for (int j = 0; j < x.n_cols; ++j) {
//             grads(i, j) = Ks(i) * (x(i, j) - nx(0, j) / l2);
//         }
//     }

//     return grads;
// }