#include "../include/types.h"
#include "../include/GPR.h"
#include <cuda.h>
#include <cublas_v2.h>
#include <cuda_runtime.h>
#include <cusolverDn.h>
#include <stdio.h>

extern const int THREADS_PER_BLOCK;

#define CUDA_CALL(x) do { cudaError_t err = x; \
    if ((err)!=cudaSuccess) {   \
    printf("Error at %s:%d: %s\n", __FILE__, __LINE__, cudaGetErrorString(err));\
    return EXIT_FAILURE;}} while(0)

#define CUBLAS_CALL(x) do { if ((x)!=CUBLAS_STATUS_SUCCESS) {\
    printf("Error at %s:%d\n", __FILE__, __LINE__); \
    return EXIT_FAILURE;}} while(0)

#define CUSOLVER_CALL(x) do { if ((x)!=CUSOLVER_STATUS_SUCCESS) {\
    printf("Error at %s:%d\n", __FILE__, __LINE__); \
    return EXIT_FAILURE;}} while(0)

extern __global__ void _device_eyes(scala_t *d_m, int N, int N_sq);

extern __global__ void _device_ones(scala_t *d_m, int N);

extern __global__ void _device_diag_log(scala_t *d_m, scala_t *d_diag, int N);

extern "C" int __device_radius(scala_t* d_v1, scala_t* d_v2, scala_t *d_r, int N,
    cublasHandle_t handle);

extern "C" int __device_vv_rbf(scala_t* d_v1, scala_t* d_v2, scala_t* d_r, int N,
    scala_t l_sq, scala_t sigma_sq, cublasHandle_t handle);

extern "C" int __device_mm_rbf(scala_t* d_m, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq);

extern "C" int __device_noise_mm_rbf(scala_t* d_m, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

extern "C" int __device_noise_mv_rbf(scala_t* d_m, const scala_t *m, scala_t *d_v, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

extern "C" int inv_kernel(const scala_t* m, scala_t* s, int N) {
    // s is aux identity matrix
    scala_t *d_m, *d_s, *ws;
    int Lwork, *info;
    cusolverDnHandle_t handle;
    cudaStream_t fac_s, sol_s;
    cublasFillMode_t lo = CUBLAS_FILL_MODE_LOWER;

    CUDA_CALL(cudaStreamCreate(&fac_s));
    CUDA_CALL(cudaStreamCreate(&sol_s));
    CUSOLVER_CALL(cusolverDnCreate(&handle));

    CUDA_CALL(cudaMalloc((void**)&d_m, N*N*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_s, N*N*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&info, sizeof(int)));

    CUDA_CALL(cudaHostRegister((void*)m, N*N*sizeof(scala_t), cudaHostRegisterDefault));
    CUDA_CALL(cudaHostRegister((void*)s, N*N*sizeof(scala_t), cudaHostRegisterDefault));
    // copy matrix data
    CUDA_CALL(cudaMemcpyAsync(d_m, m, N*N*sizeof(scala_t), cudaMemcpyHostToDevice, fac_s));
    // compute workspace size and malloc workspace
    CUSOLVER_CALL(cusolverDnSetStream(handle, fac_s));
    CUSOLVER_CALL(cusolverDnDpotrf_bufferSize(handle, lo, N, d_m, N, &Lwork));
    CUDA_CALL(cudaMalloc((void **)&ws, Lwork*sizeof(scala_t)));
    // decompose
    CUSOLVER_CALL(cusolverDnDpotrf(handle, lo, N, d_m, N, ws, Lwork, info));

    // copy identity matrix
    CUSOLVER_CALL(cusolverDnSetStream(handle, sol_s));
    CUDA_CALL(cudaMemcpyAsync(d_s, s, N*N*sizeof(scala_t), cudaMemcpyHostToDevice, sol_s));
    CUDA_CALL(cudaDeviceSynchronize());
    CUSOLVER_CALL(cusolverDnDpotrs(handle, lo, N, N, d_m, N, d_s, N, info));

    CUDA_CALL(cudaDeviceSynchronize());
    // copy out
    CUDA_CALL(cudaMemcpy(s, d_s, N*N*sizeof(scala_t), cudaMemcpyDeviceToHost));

    // clean up
    CUDA_CALL(cudaHostUnregister((void*)m));
    CUDA_CALL(cudaHostUnregister((void*)s));
    CUDA_CALL(cudaFree(d_m));
    CUDA_CALL(cudaFree(d_s));
    CUDA_CALL(cudaFree(ws));
    CUDA_CALL(cudaFree(info));
    CUSOLVER_CALL(cusolverDnDestroy(handle));
    CUDA_CALL(cudaDeviceReset());
    return 0;
}

static __inline__ int _device_chol_lower(scala_t *d_m, int N, cusolverDnHandle_t handle) {
    cublasFillMode_t lo = CUBLAS_FILL_MODE_LOWER;
    scala_t *ws;
    int work_size;
    int *d_info;
    // compute workspace size and malloc workspace
    CUSOLVER_CALL(cusolverDnDpotrf_bufferSize(handle, lo, N, d_m, N, &work_size));
    CUDA_CALL(cudaMalloc((void **)&ws, work_size*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_info, sizeof(int)));
    // decompose
    CUSOLVER_CALL(cusolverDnDpotrf(handle, lo, N, d_m, N, ws, work_size, d_info));
    CUDA_CALL(cudaDeviceSynchronize());

    // clean up
    CUDA_CALL(cudaFree(ws));
    CUDA_CALL(cudaFree(d_info));

    return 0;
}

static __inline__ int _device_solve_inverse_lower(scala_t *d_chol, scala_t *d_inv, 
    int N, cusolverDnHandle_t handle) {
    cublasFillMode_t lo = CUBLAS_FILL_MODE_LOWER;
    int *d_info;
    CUDA_CALL(cudaMalloc((void **)&d_info, sizeof(int)));

    CUSOLVER_CALL(cusolverDnDpotrs(handle, lo, N, N, d_chol, N, d_inv,  N, d_info));
    CUDA_CALL(cudaDeviceSynchronize());
    CUDA_CALL(cudaFree(d_info));
    return 0;
}

extern "C" int _device_loglik(const scala_t* x, const scala_t*y,
    scala_t *loglik, int row, int col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    scala_t *d_x, *d_k, *d_inv, *d_y;
    CUDA_CALL(cudaMalloc((void **)&d_x, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_k, col*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_y, col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_inv, col*col*sizeof(scala_t)));

    // prepare data 
    CUDA_CALL(cudaMemcpy(d_x, x, row*col*sizeof(scala_t), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy(d_y, y, col*sizeof(scala_t), cudaMemcpyHostToDevice));
    // kernel
    // __device_mm_rbf(d_x, d_k, row, col, l_sq, sigma_sq);
    __device_noise_mm_rbf(d_x, d_k, row, col, l_sq, sigma_sq, noise_sq);

    // chol
    cusolverDnHandle_t solver_handle;
    CUSOLVER_CALL(cusolverDnCreate(&solver_handle));
    _device_chol_lower(d_k, col, solver_handle);

    // inverse
    cublasHandle_t blas_handle;
    CUBLAS_CALL(cublasCreate(&blas_handle));

    int EYE_BLOCK = (col*col + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
    _device_eyes<<<EYE_BLOCK, THREADS_PER_BLOCK>>>(d_inv, col, col*col);
    _device_solve_inverse_lower(d_k, d_inv, col, solver_handle);

    // alpha and a
    scala_t *d_aux_y;
    scala_t a;
    CUDA_CALL(cudaMalloc((void **)&d_aux_y, col*sizeof(scala_t)));
    CUDA_CALL(cudaMemcpy(d_aux_y, d_y, col*sizeof(scala_t), cudaMemcpyDeviceToDevice));
    scala_t alpha = 1., beta = 0.;
    CUBLAS_CALL(cublasDgemv(blas_handle, CUBLAS_OP_N, col, col, &alpha, d_inv, col, d_y, 1, &beta, d_aux_y, 1));
    CUBLAS_CALL(cublasDdot(blas_handle, col, d_aux_y, 1, d_y, 1, &a)); // a

    // log det
    scala_t logdet;
    scala_t *d_logdet, *d_aux_logdet;
    CUDA_CALL(cudaMalloc((void **)&d_logdet, col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_aux_logdet, col*sizeof(scala_t)));

    int ONES_BLOCK = (col + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
    _device_diag_log<<<ONES_BLOCK, THREADS_PER_BLOCK>>>(d_k, d_logdet, col);
    _device_ones<<<ONES_BLOCK, THREADS_PER_BLOCK>>>(d_aux_logdet, col);
    CUBLAS_CALL(cublasDdot(blas_handle, col, d_aux_logdet, 1, d_logdet, 1, &logdet));

    (*loglik) = -0.5 * a - logdet - 0.5 * scala_t(row) * log(2. * M_PI);

    // clean up
    CUBLAS_CALL(cublasDestroy(blas_handle));
    CUSOLVER_CALL(cusolverDnDestroy(solver_handle));
    CUDA_CALL(cudaFree(d_x));
    CUDA_CALL(cudaFree(d_k));
    CUDA_CALL(cudaFree(d_inv));
    CUDA_CALL(cudaFree(d_y));
    CUDA_CALL(cudaFree(d_aux_y));
    CUDA_CALL(cudaFree(d_logdet));
    CUDA_CALL(cudaFree(d_aux_logdet));
    return 0;
}

extern "C" int _device_mu(const scala_t *m, const scala_t *x, const scala_t *y,
    scala_t *mu, int row, int col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    scala_t *d_m, *d_x, *d_y;
    scala_t *d_k, *d_inv, *d_ks;

    CUDA_CALL(cudaMalloc((void **)&d_m, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_x, col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_y, col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_k, col*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_ks, col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void **)&d_inv, col*col*sizeof(scala_t)));
    
    // copy data
    // CUDA_CALL(cudaMemcpy(d_m, m, row*col*sizeof(double), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy(d_x, x, col*sizeof(double), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy(d_y, y, col*sizeof(double), cudaMemcpyHostToDevice));

    // compute mv kernel
    __device_noise_mv_rbf(d_m, m, d_x, d_ks, row, col, l_sq, sigma_sq, noise_sq);

    // compute kernel
    __device_noise_mm_rbf(d_m, d_k, row, col, l_sq, sigma_sq, noise_sq);
    
    // chol
    cusolverDnHandle_t solver_handle;
    CUSOLVER_CALL(cusolverDnCreate(&solver_handle));
    _device_chol_lower(d_k, col, solver_handle);

    // inverse
    int EYE_BLOCK = (col*col + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
    _device_eyes<<<EYE_BLOCK, THREADS_PER_BLOCK>>>(d_inv, col, col*col);
    _device_solve_inverse_lower(d_k, d_inv, col, solver_handle);

    // alpha
    cublasHandle_t blas_handle;
    CUBLAS_CALL(cublasCreate(&blas_handle));
    scala_t alpha = 1., beta = 0.;
    CUBLAS_CALL(cublasDgemv(blas_handle, CUBLAS_OP_N, col, col, &alpha, d_inv, col, d_y, 1, &beta, d_y, 1));

    // mu
    CUBLAS_CALL(cublasDdot(blas_handle, col, d_ks, 1, d_y, 1, mu));

    // clean up
    CUBLAS_CALL(cublasDestroy(blas_handle));
    CUSOLVER_CALL(cusolverDnDestroy(solver_handle));
    CUDA_CALL(cudaFree(d_ks));
    CUDA_CALL(cudaFree(d_inv));
    CUDA_CALL(cudaFree(d_k));
    CUDA_CALL(cudaFree(d_y));
    CUDA_CALL(cudaFree(d_x));
    CUDA_CALL(cudaFree(d_m));

    return 0;
}