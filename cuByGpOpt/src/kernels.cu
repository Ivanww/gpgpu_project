#include "../include/types.h"
#include "../include/kernels.h"
#include <cuda.h>
#include <cublas_v2.h>
#include <cuda_runtime.h>
#include <iostream>

#define CUDA_CALL(x) do { cudaError_t err = x; \
    if ((err)!=cudaSuccess) {   \
    printf("Error at %s:%d: %s\n", __FILE__, __LINE__, cudaGetErrorString(err));\
    return EXIT_FAILURE;}} while(0)

#define CUBLAS_CALL(x) do { if ((x)!=CUBLAS_STATUS_SUCCESS) {\
    printf("Error at %s:%d\n", __FILE__, __LINE__); \
    return EXIT_FAILURE;}} while(0)

extern __global__ void rbf(scala_t *d_value, scala_t l_sq, scala_t sigma_sq, int N);

__global__ void  grad_l(scala_t *d_r, scala_t l_sq, scala_t sigma_sq, int N) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    if (index < N) {
        d_r[index] = d_r[index] * d_r[index] / l_sq;
        d_r[index] = d_r[index] * sigma_sq * exp(-0.5 * d_r[index]);
    }
}

__global__ void grad_sigma(scala_t *d_r, scala_t l_sq, scala_t sigma_sq, int N) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    if (index < N) {
        d_r[index] = d_r[index] * d_r[index] / l_sq;
        d_r[index] = 2. * sigma_sq * exp(-0.5 * d_r[index]);
    }
}

extern "C" int __device_radius(scala_t* d_v1, scala_t* d_v2, scala_t *d_r, int N,
    cublasHandle_t handle);

extern "C" int __device_vv_rbf(scala_t* d_v1, scala_t* d_v2, scala_t* d_k, int N, 
    scala_t l_sq, scala_t sigma_sq, cublasHandle_t handle);

extern "C" int __device_mm_rbf(scala_t* d_m, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq);

extern "C" int __device_noise_mv_rbf(scala_t* d_m, scala_t* m, scala_t* d_v, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

extern "C" int __device_noise_mm_rbf(scala_t* d_m, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

// compute 
static int vv_noise_kernel(const scala_t* v1, const scala_t* v2, scala_t* k,
    int N, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    cublasHandle_t handle;
    scala_t *d_v1, *d_v2;// *d_k;
    // malloc space
    CUDA_CALL(cudaMalloc((void**)&d_v1, N*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_v2, N*sizeof(scala_t)));
    // init handle
    CUBLAS_CALL(cublasCreate(&handle));

    // copy data
    CUDA_CALL(cudaMemcpy(d_v2, v2, N*sizeof(scala_t), cudaMemcpyHostToDevice));
    CUDA_CALL(cudaMemcpy(d_v1, v1, N*sizeof(scala_t), cudaMemcpyHostToDevice));

    // compute kernel
    scala_t a = -1.;
    CUBLAS_CALL(cublasDaxpy(handle, N, &a, d_v2, 1, d_v1, 1)); // x1 = -x2 + x1
    CUBLAS_CALL(cublasDnrm2(handle, N, d_v1, 1, k));
    (*k) = sigma_sq * exp(-0.5 * (*k) * (*k) / l_sq) + noise_sq; // this is a CPU operation

    // clean up
    CUBLAS_CALL(cublasDestroy(handle));
    CUDA_CALL(cudaFree(d_v1));
    CUDA_CALL(cudaFree(d_v2));

    return EXIT_SUCCESS;
}

static int mv_noise_kernel(const scala_t* m1, const scala_t* v2, scala_t* k,
    int row, int col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    scala_t *d_m1, *d_v2, *d_k;
    // copy v2 in default stream 
    CUDA_CALL(cudaMalloc((void**)&d_v2, row*sizeof(scala_t)));
    CUDA_CALL(cudaMemcpy(d_v2, v2, row*sizeof(scala_t), cudaMemcpyHostToDevice));

    // malloc device space
    CUDA_CALL(cudaMalloc((void**)&d_m1, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_k, col*sizeof(scala_t)));
    
    // compute kernel
    __device_noise_mv_rbf(d_m1, const_cast<scala_t *>(m1), d_v2, d_k, row, col, l_sq, sigma_sq, noise_sq);

    // copy data
    CUDA_CALL(cudaMemcpy(k, d_k, col*sizeof(scala_t), cudaMemcpyDeviceToHost));

    // clean up
    CUDA_CALL(cudaFree(d_m1));
    CUDA_CALL(cudaFree(d_v2));
    CUDA_CALL(cudaFree(d_k));

    return EXIT_SUCCESS;
}

static int mm_noise_kernel(const scala_t* m, scala_t* k, int row, int col, 
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    scala_t *d_m, *d_k;

    CUDA_CALL(cudaMalloc((void**)&d_m, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_k, col*col*sizeof(scala_t)));
    CUDA_CALL(cudaMemcpy(d_m, m, row*col*sizeof(scala_t), cudaMemcpyHostToDevice));
    
//    __device_mm_rbf(d_m, d_k, row, col, l_sq, sigma_sq); 
   __device_noise_mm_rbf(d_m, d_k, row, col, l_sq, sigma_sq, noise_sq); 

    CUDA_CALL(cudaMemcpy(k, d_k, col*col*sizeof(scala_t), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaFree(d_m));
    CUDA_CALL(cudaFree(d_k));
    return 0;
}

// wrapper functions
extern "C" int _device_kernel(const scala_t* x1, const scala_t* x2, scala_t* k,
    int x1_col, int row, int x2_col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    if (x1_col == 1 && x2_col == 1) {
        return vv_noise_kernel(x1, x2, k, row, l_sq, sigma_sq, noise_sq);
    }
    else if (x2_col == 1) {
        return mv_noise_kernel(x1, x2, k, row, x1_col, l_sq, sigma_sq, noise_sq);
    }
    else {
        // assume x1 and x2 are the same
        return mm_noise_kernel(x1, k, row, x1_col, l_sq, sigma_sq, noise_sq);
    }
}

extern "C" int _device_kernel_gradient(const scala_t* m1, const scala_t * v2, scala_t* t_grad, 
    int row, int col, scala_t l_sq, scala_t sigma_sq) {
    scala_t *d_m1, *d_v2, *d_grad;
    cudaStream_t *streams = (cudaStream_t *)malloc(col*sizeof(cudaStream_t));
    cudaStream_t dl_s, dsigma_s;
    cublasHandle_t handle;
    // copy v2 in default stream 
    CUDA_CALL(cudaMalloc((void**)&d_v2, row*sizeof(scala_t)));
    CUDA_CALL(cudaMemcpy(d_v2, v2, row*sizeof(scala_t), cudaMemcpyHostToDevice));
    CUBLAS_CALL(cublasCreate(&handle));

    // launch streams
    for (int i = 0; i < col; ++i) {
        CUDA_CALL(cudaStreamCreate(&streams[i]));
    }
    CUDA_CALL(cudaStreamCreate(&dl_s));
    CUDA_CALL(cudaStreamCreate(&dsigma_s));
    // malloc matrix space and grad space 
    CUDA_CALL(cudaMalloc((void**)&d_m1, row*col*sizeof(scala_t)));
    CUDA_CALL(cudaMalloc((void**)&d_grad, 2*col*sizeof(scala_t)));
    
    CUDA_CALL(cudaHostRegister((void*)m1, row*col*sizeof(scala_t), cudaHostRegisterDefault));
    // copy data and compute radius 
    for (int i = 0; i < col; ++i) {
        CUBLAS_CALL(cublasSetStream(handle, streams[i]));
        CUDA_CALL(cudaMemcpyAsync(d_m1+i*row, m1+i*row, row*sizeof(scala_t), cudaMemcpyHostToDevice, streams[i]));
        __device_radius(d_m1+i*row, d_v2, d_grad+i, row, handle);
    }
    // copy radius and calculate gradients
    CUDA_CALL(cudaDeviceSynchronize());
    CUDA_CALL(cudaMemcpy(d_grad+col, d_grad, col*sizeof(scala_t), cudaMemcpyDeviceToDevice));

    int BLOCK = (col+THREADS_PER_BLOCK-1) / THREADS_PER_BLOCK;
    grad_l<<<BLOCK, THREADS_PER_BLOCK, 0, dl_s>>>(d_grad, l_sq, sigma_sq, col);
    grad_sigma<<<BLOCK, THREADS_PER_BLOCK, 0, dsigma_s>>>(d_grad+col, l_sq, sigma_sq, col);

    // copy out
    CUDA_CALL(cudaDeviceSynchronize());
    CUDA_CALL(cudaMemcpy(t_grad, d_grad, 2*col*sizeof(scala_t), cudaMemcpyDeviceToHost));
    // clean up
    for (int i = 0; i < col; ++i) {
        CUDA_CALL(cudaStreamDestroy(streams[i]));
    }
    CUDA_CALL(cudaStreamDestroy(dl_s));
    CUDA_CALL(cudaStreamDestroy(dsigma_s));
    CUBLAS_CALL(cublasDestroy(handle));
    CUDA_CALL(cudaHostUnregister((void*)m1));
    CUDA_CALL(cudaFree(d_m1));
    CUDA_CALL(cudaFree(d_v2));
    CUDA_CALL(cudaFree(d_grad));
    free(streams);

    return 0;
}