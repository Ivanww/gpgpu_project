#include "../include/BO.h"
#include "../include/acquisition.h"
#include <nlopt.hpp>

void BO::run(int n_iter, int n_starts) {
    for (int i = 0; i < n_iter; ++i) {
        m_model.fit();

        vector_t proposed = propose(n_starts);
        matrix_t new_obervation = m_objective(proposed.t());
        std::cout << "[BO] new proposed location " << proposed(0) << std::endl;
        std::cout << "[BO] new observation: " << new_obervation << std::endl;
        m_model.add_data(proposed, new_obervation);
    }
}

row_t BO::propose(int n_starts) {
    // random and rescale initial starting points
    matrix_t init_starts(m_model.sample_dim(), n_starts, arma::fill::randu);
    // rescale starting points
    for (index_t i = 0; i < m_model.sample_dim(); ++i) {
        scala_t scale = m_upper_bounds[i] - m_lower_bounds[i];
        init_starts.row(i) = init_starts.row(i) * scale + m_lower_bounds[i];
    }
    vector_t proposed(m_model.sample_dim(), arma::fill::zeros);
    scala_t maxf = -arma::datum::inf;
    for (index_t i = 0; i < n_starts; ++i) {
        std_vector_t init(init_starts.col(i).begin(), init_starts.col(i).end());
        std::cout << "Init point " << init[0] << std::endl;
        nlopt::opt opt(nlopt::GN_DIRECT_L_RAND, m_model.sample_dim());    
        opt.set_lower_bounds(m_lower_bounds);
        opt.set_upper_bounds(m_upper_bounds);
        opt.set_maxeval(80);

        ACQobj obj(m_ei);
        opt.set_max_objective(ACQobj::wrap, &obj);
        scala_t lmax;

        try {
            opt.optimize(init, lmax);
        }
        catch (nlopt::roundoff_limited& e) {
            std::cerr << "[NLOPT] " << e.what() << std::endl;
        }
        catch (std::invalid_argument e) {
            std::cerr << "[NLOPT] " << e.what() << std::endl;
        }
        catch (std::runtime_error& e) {
            std::cerr << "[NLOPT] " << e.what() << std::endl;
        }
        std::cout << "[NLOPT] max LL: " << lmax << std::endl;
        std::cout << "[NLOPT] ll location (" << init[0] << ")" << std::endl;

        if (lmax > maxf) {
            maxf = lmax;
            proposed = init;
        }
    }
    return proposed;
}