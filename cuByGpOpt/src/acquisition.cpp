#include "../include/acquisition.h"
#include <cmath>
#include <cassert>

EI::EI(GPR& model) : m_model(model), m_xi(0.0) {}
EI::EI(GPR& model, scala_t xi) : m_model(model), m_xi(xi) {}

EI::~EI() {}

scala_t EI::operator()(const row_t& x) {
    matrix_t mu = m_model.mu(x);
    matrix_t sigma_sq = m_model.sigma(x);
    if (sigma_sq(0, 0) < 1e-8) {
        return 0.;
    }
    scala_t max_mu = m_model.max_observation();
    scala_t improve = mu(0, 0)-max_mu-m_xi;
    scala_t Z = improve / std::sqrt(sigma_sq(0, 0));
    scala_t cdf = arma::normcdf(Z);
    scala_t pdf = arma::normpdf(Z);
    scala_t ei = improve * cdf + std::sqrt(sigma_sq(0, 0)) * pdf;

    return ei;
}

// matrix_t EI::gradient(const matrix_t& x) {
//     std::vector<matrix_t> pred = mRefGPR.predict_with_gradient(x, true);
//     scala_t xi = 1e-8;
//     scala_t min_mu = mRefGPR.minY();
//     scala_t improve =  pred[0](0, 0)-min_mu-xi;
//     scala_t Z = improve / pred[1](0, 0);
//     scala_t cdf = arma::normcdf(Z);
//     scala_t pdf = arma::normpdf(Z);

//     matrix_t improve_grad = (- pred[2].t() * pred[1](0, 0) - pred[3] * improve) / (pred[1](0, 0) * pred[1](0, 0));
//     matrix_t cdf_grad = improve_grad * pdf;
//     matrix_t pdf_grad = -improve * cdf_grad;
//     matrix_t exploit_grad = - pred[2].t() * cdf - pdf_grad;
//     matrix_t explore_grad = pred[3] * pdf + pdf_grad;
//     matrix_t grad = exploit_grad + explore_grad;

//     return grad;
// }

scala_t ACQobj::operator()(const std_vector_t& x, std_vector_t& grad) {
    assert(grad.empty());
    if (!grad.empty()) {
        // ei gradient
    }
    return m_ei(x);
}
