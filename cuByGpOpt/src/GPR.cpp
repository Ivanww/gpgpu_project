#include "../include/GPR.h"
#include <cassert>
#include <nlopt.hpp>
#include <iomanip>
#include <limits>

#include <iostream>

Nllobj::Nllobj(GPR& model) : mGPR(model) {}
Nllobj::~Nllobj() {}
scala_t Nllobj::operator()(const std_vector_t& x, std_vector_t& grad) {
    mGPR.kernel().params(x);
    if (!grad.empty()) {
        // matrix_t analytic = mGPR.gradient_loglik();
        matrix_t analytic = mGPR.gradient_loglik_raw();
        grad[0] = -analytic(0, 0);
        grad[1] = -analytic(0, 1);
        // matrix_t numeric = mGPR.gradient_loglik_numeric();
        // grad[0] = -numeric(0, 0);
        // grad[1] = -numeric(0, 1);
    }
    return -mGPR.loglik();
}

GPR::GPR() : m_kern() {}
GPR::GPR(RBFKernel& k) : m_kern(k.params()) {}
GPR::~GPR() {}

void GPR::fit(const matrix_t& x, const vector_t& y) {
    m_samples = x;
    m_observations = y;
    fit();
}

void GPR::fit() {
    // nlopt::opt opt(nlopt::LD_LBFGS, );
    nlopt::opt opt(nlopt::LN_COBYLA, 2);
    Nllobj nllobj(*this); 
    std_vector_t x = m_kern.params(); 
    opt.set_min_objective(Nllobj::wrap, &nllobj);
    opt.set_maxeval(100);
    // std_vector_t lb({-1., -1.});
    // std_vector_t ub({1, 1.});
    // opt.set_lower_bounds(lb);
    // opt.set_upper_bounds(ub);
    // opt.set_ftol_rel(-1);
    // opt.set_xtol_rel(-1);
    double minf;
    try {
        opt.optimize(x, minf);
    }
    catch (nlopt::roundoff_limited& e) {
        std::cerr << "[NLOPT] " << e.what() << std::endl;
    }
    catch (std::invalid_argument e) {
        std::cerr << "[NLOPT] " << e.what() << std::endl;
    }
    catch (std::runtime_error& e) {
        std::cerr << "[NLOPT] " << e.what() << std::endl;
    }
    m_kern.params(x);
}

matrix_t GPR::mu(const matrix_t& vs) {
    matrix_t mu_mat(1, vs.n_cols);
    matrix_t K = m_kern(m_samples, m_samples);
    matrix_t alpha = K.i() * m_observations;
    for (index_t i = 0; i < vs.n_cols; ++i) {
        matrix_t ks_mat = m_kern(m_samples, vs.col(i));
        mu_mat.col(i) = (ks_mat * alpha);
    }
    return mu_mat;
}

extern "C" int _device_mu(const scala_t *m, const scala_t *x, const scala_t *y,
    scala_t *mu, int row, int col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);

matrix_t GPR::cu_mu(const matrix_t& vs) {
    assert(vs.n_cols == 1);
    matrix_t mu_mat(1, 1);
    _device_mu(m_samples.memptr(), vs.memptr(), m_observations.memptr(), mu_mat.memptr(),
        m_samples.n_rows, m_samples.n_cols, m_kern.l(), m_kern.sigma(), m_kern.noise());
    return mu_mat;
}

matrix_t GPR::sigma(const matrix_t& vs) {
    matrix_t sigma_mat(1, vs.n_cols);
    matrix_t k_mat = m_kern(m_samples, m_samples);
    matrix_t kl_mat = arma::chol(k_mat, "lower");
    for (index_t i = 0; i < vs.n_cols; ++i) {
        matrix_t ks_mat = m_kern(m_samples, vs.col(i));
        matrix_t lk_mat = arma::solve(kl_mat, ks_mat.t());
        matrix_t kss_mat = m_kern(vs.col(i), vs.col(i));
        sigma_mat.col(i) = kss_mat - lk_mat.t() * lk_mat;
        if (sigma_mat(0, i) <= std::numeric_limits<scala_t>::epsilon()) {
            sigma_mat(0, i) = 0.;
        }
    }
    return sigma_mat;// + m_kern.noise();
}

matrix_t GPR::cu_sigma(const matrix_t& vs) {

}

// not working on average observations
scala_t GPR::loglik() {
    matrix_t k_mat = m_kern(m_samples, m_samples);
    matrix_t kl_mat = arma::chol(k_mat, "lower");
    matrix_t alpha = k_mat.i() * m_observations;
    scala_t logdet = 2. * arma::accu(arma::log(kl_mat.diag()));
    scala_t a = arma::trace(m_observations.t() * alpha); // trace of 1x1 matrix
    return -0.5 * a - 0.5 * logdet - 0.5 * float(m_samples.n_rows) *  std::log(2. * M_PI);
}

extern "C" int _device_loglik(const scala_t* x, const scala_t*y,
    scala_t *loglik, int row, int col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq);
scala_t GPR::cu_loglik() {
    scala_t loglik;
    _device_loglik(m_samples.memptr(), m_observations.memptr(), &loglik, m_samples.n_rows,
        m_samples.n_cols, m_kern.l(), m_kern.sigma(), m_kern.noise());
    return loglik;
}

matrix_t GPR::gradient_loglik() {
    matrix_t k_mat = m_kern(m_samples, m_samples);
    matrix_t kinv_mat = k_mat.i();
    matrix_t alpha = kinv_mat * m_observations;
    matrix_t w = alpha * alpha.t() - kinv_mat;
    matrix_t grads(2, 1, arma::fill::zeros);
    index_t N = m_observations.n_rows;
    for (index_t i = 0; i < N; ++i) {
        matrix_t g = m_kern.gradient_params(m_samples, m_samples.col(i));
        for (index_t j = 0; j <= i; ++j) {
            if (i == j) {
                grads += w(i, j) * g.col(j) * 0.5;
            }
            else {
                grads += w(i, j) * g.col(j);
            }
        }
    }
    return grads;
}


matrix_t GPR::gradient_loglik_raw() { 
    matrix_t k_mat = m_kern(m_samples, m_samples);
    matrix_t kinv_mat = k_mat.i();
    matrix_t dl_mat = m_kern.gradient_l(m_samples, m_samples);
    matrix_t dsigma_mat = m_kern.gradient_sigma(m_samples, m_samples);
    matrix_t alpha = kinv_mat * m_observations;
    matrix_t grads(2, 1);
    grads(0, 0) = 0.5 * arma::trace((alpha*alpha.t() - kinv_mat) * dl_mat);
    grads(1, 0) = 0.5 * arma::trace((alpha*alpha.t() - kinv_mat) * dsigma_mat);

    return grads;
}

matrix_t GPR::gradient_loglik_numeric() {
    scala_t delta = 1e-3;
    std_vector_t param_copy = m_kern.params();
    matrix_t grads(1, 2);
    
    // gradient of l
    param_copy[0] -= delta;
    m_kern.params(param_copy);
    scala_t res1 = loglik();
    param_copy[0] += 2.*delta;
    m_kern.params(param_copy);
    scala_t res2 = loglik();
    param_copy[0] -= delta;
    grads(0, 0) = (res2 - res1) / (2.*delta);

    // gradient of sigma_sq
    param_copy[1] -= delta;
    m_kern.params(param_copy);
    scala_t res3 = loglik();
    param_copy[1] += (2.*delta);
    m_kern.params(param_copy);
    scala_t res4 = loglik();
    param_copy[1] -= delta;
    grads(0, 1) = (res3 - res4) / (2.*delta);

    // recover parameters;
    m_kern.params(param_copy);

    return grads;
}

scala_t GPR::min_observation() {
    return m_observations.min();
}

scala_t GPR::max_observation() {
    return m_observations.max();
}

void GPR::add_data(const matrix_t& n_samples, const vector_t& n_observations) {
    m_samples.insert_cols(m_samples.n_cols, n_samples);
    m_observations.insert_rows(m_observations.n_rows, n_observations);
}
