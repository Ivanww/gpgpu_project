#include "../include/types.h"
#include "../include/GPR.h"
#include <cuda.h>
#include <cublas_v2.h>
#include <cuda_runtime.h>
#include <cusolverDn.h>
#include <curand.h>

// static const int THREADS_PER_BLOCK = 128;
extern const int THREADS_PER_BLOCK;

#define CUDA_CALL(x) do { cudaError_t err = x; \
    if ((err)!=cudaSuccess) {   \
    printf("Error at %s:%d: %s\n", __FILE__, __LINE__, cudaGetErrorString(err));\
    return EXIT_FAILURE;}} while(0)

#define CUBLAS_CALL(x) do { if ((x)!=CUBLAS_STATUS_SUCCESS) {\
    printf("Error at %s:%d\n", __FILE__, __LINE__); \
    return EXIT_FAILURE;}} while(0)

#define CUSOLVER_CALL(x) do { if ((x)!=CUSOLVER_STATUS_SUCCESS) {\
    printf("Error at %s:%d\n", __FILE__, __LINE__); \
    return EXIT_FAILURE;}} while(0)

#define CURAND_CALL(x) do { if ((x)!=CURAND_STATUS_SUCCESS) {\
    printf("Error at %s:%d\n", __FILE__, __LINE__); \
    return EXIT_FAILURE;}} while(0)
// elementwise operation
__global__ void rbf(scala_t *d_value, scala_t l_sq, scala_t sigma_sq, int N) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    if (index < N) {
        d_value[index] = sigma_sq * exp(-0.5 * d_value[index] * d_value[index] / l_sq);
    }
}
__global__ void _device_eyes(scala_t *d_m, int N, int N_sq) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    if (index < N_sq) {
        d_m[index] = (index / N == index % N) ? 1. : 0.;
    }
}

__global__ void _device_ones(scala_t *d_m, int N) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    if (index < N) {
        d_m[index] = 1.;
    }
}

__global__ void _device_diag_log(scala_t *d_m, scala_t *d_diag, int N) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    if (index / N == index % N) {
        d_diag[index % N] = log(d_m[index]);
    }
}

extern "C" int __device_radius(scala_t* d_v1, scala_t* d_v2, scala_t *d_r, int N,
    cublasHandle_t handle) {
    scala_t a = -1;
    CUBLAS_CALL(cublasDaxpy(handle, N, &a, d_v2, 1, d_v1, 1));
    CUBLAS_CALL(cublasSetPointerMode(handle, CUBLAS_POINTER_MODE_DEVICE));
    CUBLAS_CALL(cublasDnrm2(handle, N, d_v1, 1, d_r));
    CUBLAS_CALL(cublasSetPointerMode(handle, CUBLAS_POINTER_MODE_HOST));
    return 0;
}
extern "C" int __mixed_noise_vv_rbf(scala_t *d_v1, scala_t* d_v2, scala_t* k, int N,
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq, cublasHandle_t handle) {
    scala_t a = -1;
    CUBLAS_CALL(cublasDaxpy(handle, N, &a, d_v2, 1, d_v1, 1));
    CUBLAS_CALL(cublasDnrm2(handle, N, d_v1, 1, k));

    (*k) = sigma_sq * exp(-0.5 * (*k) * (*k) / l_sq) + noise_sq; // this is a CPU operation

    return 0;
}
extern "C" int __device_vv_rbf(scala_t* d_v1, scala_t* d_v2, scala_t* d_k, int N, 
    scala_t l_sq, scala_t sigma_sq, cublasHandle_t handle) {
    int BLOCK = (N+THREADS_PER_BLOCK-1) / THREADS_PER_BLOCK;
    cudaStream_t m_stream;
    CUBLAS_CALL(cublasGetStream(handle, &m_stream));
    __device_radius(d_v1, d_v2, d_k, N, handle);
    rbf<<<BLOCK, THREADS_PER_BLOCK, 0, m_stream>>>(d_k, l_sq, sigma_sq, 1);
    return EXIT_SUCCESS;
}

extern "C" int __device_mv_rbf(scala_t* d_m, const scala_t *m, scala_t* d_v, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq) {
    cudaStream_t *streams = (cudaStream_t *)malloc(col*sizeof(cudaStream_t));
    cublasHandle_t handle;
    CUBLAS_CALL(cublasCreate(&handle));

   // launch streams
    for (int i = 0; i < col; ++i) {
        CUDA_CALL(cudaStreamCreate(&streams[i]));
    }

    // register memory
    CUDA_CALL(cudaHostRegister((void*)m, row*col*sizeof(scala_t), cudaHostRegisterDefault));

    for (int i = 0; i < col; ++i) {
        // copy col
        CUBLAS_CALL(cublasSetStream(handle, streams[i]));
        CUDA_CALL(cudaMemcpyAsync(d_m+i*row, m+i*row, row*sizeof(scala_t), cudaMemcpyHostToDevice, streams[i]));
        // compute
        __device_vv_rbf(d_m+i*row, d_v, d_k+i, row, l_sq, sigma_sq, handle);
    }

    // clean up
    CUDA_CALL(cudaDeviceSynchronize());
    CUDA_CALL(cudaHostUnregister((void*)m));
    CUBLAS_CALL(cublasDestroy(handle));
    for (int i = 0; i < col; ++i) {
        CUDA_CALL(cudaStreamDestroy(streams[i]));
    }
    free(streams);

    return 0;
}

extern "C" int __device_noise_mv_rbf(scala_t* d_m, const scala_t* m, scala_t* d_v, scala_t* d_k,
    int row, int col, scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    scala_t *d_noise;
    scala_t noise = noise_sq;
    CUDA_CALL(cudaMalloc((void **)&d_noise, col*sizeof(scala_t)));
    int EYE_BLOCK = (col + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
    _device_eyes<<<EYE_BLOCK, THREADS_PER_BLOCK>>>(d_noise, col, col);
    
    __device_mv_rbf(d_m, m, d_v, d_k, row, col, l_sq, sigma_sq);

    cublasHandle_t noise_handle;
    CUBLAS_CALL(cublasCreate(&noise_handle));
    CUBLAS_CALL(cublasDaxpy(noise_handle, col, &noise, d_noise, 1, d_k, 1));
    CUBLAS_CALL(cublasDestroy(noise_handle));

    CUDA_CALL(cudaFree(d_noise));
    return 0;
}

extern "C" int __device_mm_rbf(scala_t* d_m, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq) {
    cudaStream_t *streams = (cudaStream_t *)malloc(col*sizeof(cudaStream_t));
    scala_t *d_aux;
    CUDA_CALL(cudaMalloc((void**)&d_aux, row*col*sizeof(scala_t)));
    cublasHandle_t handle;
    CUBLAS_CALL(cublasCreate(&handle));
    for (int i = 0; i < col; ++i) {
        CUDA_CALL(cudaStreamCreate(&streams[i]));
    }

    for (int i = 0; i < col; ++i) {
        for (int j = 0; j < col; ++j) {
            // copy the same col to aux
            CUDA_CALL(cudaMemcpyAsync(d_aux+j*row, d_m+i*row, row*sizeof(scala_t), cudaMemcpyDeviceToDevice));
        }
        for (int j = 0; j < col; ++j) {
            CUBLAS_CALL(cublasSetStream(handle, streams[j]));
            __device_vv_rbf(d_aux+j*row, d_m+j*row, d_k+(i*col+j), row, l_sq, sigma_sq, handle);
        }
        CUDA_CALL(cudaDeviceSynchronize());
    }
    CUBLAS_CALL(cublasDestroy(handle));
    for (int i = 0; i < col; ++i) {
        CUDA_CALL(cudaStreamDestroy(streams[i]));
    }
    CUDA_CALL(cudaFree(d_aux));
    free(streams);
    return 0;
}

extern "C" int __device_noise_mm_rbf(scala_t* d_m, scala_t* d_k, int row, int col,
    scala_t l_sq, scala_t sigma_sq, scala_t noise_sq) {
    scala_t *d_noise;
    scala_t noise = noise_sq;
    // cudaStream_t rand_s;
    // CUDA_CALL(cudaStreamCreate(&rand_s));
    CUDA_CALL(cudaMalloc((void **)&d_noise, col*col*sizeof(scala_t)));
    // __device_randu(d_noise, col, col);
    int EYE_BLOCK = (col*col + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
    _device_eyes<<<EYE_BLOCK, THREADS_PER_BLOCK>>>(d_noise, col, col*col);

    __device_mm_rbf(d_m, d_k, row, col, l_sq, sigma_sq);
    // add noise
    cublasHandle_t noise_handle;
    CUBLAS_CALL(cublasCreate(&noise_handle));
    CUBLAS_CALL(cublasDaxpy(noise_handle, col*col, &noise, d_noise, 1, d_k, 1));
    CUBLAS_CALL(cublasDestroy(noise_handle));
    // CUDA_CALL(cudaStreamDestroy(rand_s));
    CUDA_CALL(cudaFree(d_noise));
    return 0;
}

extern "C" int __device_randu(scala_t* d_m, scala_t row, scala_t col) {
    curandGenerator_t gen;
    CURAND_CALL(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
    // CURAND_CALL(curandSetStream(gen, s));
    CURAND_CALL(curandSetPseudoRandomGeneratorSeed(gen, 1234ULL));
    CURAND_CALL(curandGenerateUniformDouble(gen, d_m, row*col));
    CURAND_CALL(curandDestroyGenerator(gen));
    return 0;
}
